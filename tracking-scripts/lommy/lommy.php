#!/usr/bin/php -q
<?php
require_once("../www/func.php");
// CONFIG
$port = 30001;

/**
  * Listens for requests and forks on each connection
  */

$__server_listening = true;

error_reporting(E_ALL);
set_time_limit(0);
ob_implicit_flush();
declare(ticks = 1); // Needed for pcntl_signal

become_daemon();
change_identity(1001, 1001);

/* handle signals */
pcntl_signal(SIGTERM, 'sig_handler');
pcntl_signal(SIGINT, 'sig_handler');
pcntl_signal(SIGCHLD, 'sig_handler');

server_loop($port);

// Change the identity to a non-priv user
function change_identity($uid, $gid)
{
    if(!posix_setgid($gid))
        die("Unable to setgid to ".$gid."!\n");

    if(!posix_setuid($uid))
        die("Unable to setuid to ".$uid."!\n");
}

/**
  * Creates a server socket and listens for incoming client connections
  * @param string $address The address to listen on
  * @param int $port The port to listen on
  */
function server_loop($port)
{
    GLOBAL $__server_listening;

    if(($sock = socket_create(AF_INET, SOCK_STREAM, 0)) < 0)
        die("Failed to create socket: ".socket_strerror($sock)."\n");

    if(($ret = socket_bind($sock, 0, $port)) < 0) 
        die("Failed to bind socket: ".socket_strerror($ret)."!\n");

    if(($ret = socket_listen($sock, 0)) < 0)
        die("Failed to listen to socket: ".socket_strerror($ret)."!\n");

    socket_set_nonblock($sock);

    echo "Waiting for clients to connect\n";

    while ($__server_listening)
    {
        $connection = @socket_accept($sock);

        if ($connection === false)
            usleep(100);

        elseif ($connection > 0)
            handle_client($sock, $connection);

        else
            die("Error: ".socket_strerror($connection));
    }
}

/**
* Signal handler
*/
function sig_handler($sig) // One may wonder why
{
    switch($sig)
    {
        case SIGTERM:
        case SIGINT:
            //exit();
            break;

        case SIGCHLD:
            pcntl_waitpid(-1, $status);
            break;
    }
}

/**
* Handle a new client connection
*/
function handle_client($ssock, $csock)
{
    GLOBAL $__server_listening;

    $pid = pcntl_fork();

    /* if fork failed */
    if ($pid == -1)
        die("Fork failure!\n");
    elseif ($pid == 0)
    {
        /* child process */
        $__server_listening = false;
        socket_getpeername($csock, $remip, $remport);
        echo date("d-m-y h:i:sa")." Connection from $remip:$remport\n";
        socket_close($ssock);
        interact($csock);
        socket_close($csock);
        echo date("d-m-y h:i:sa")." Connection to $remip:$remport closed\n";
    }
    else
        socket_close($csock);
}

function interact($socket)
{
    $loopcount = 0;
    // Read the socket but don't wait for data..
    $deviceID = 0;

    while (@socket_recv($socket, $rec, 2048, 0x40) !== 0) 
    {
        // Some pacing to ensure we don't split any incoming data.
        sleep (1);

        // Timeout the socket if it's not talking...
        // Prevents duplicate connections, confusing the send commands
        $loopcount++;
        if ($loopcount > 120) return;

        // remove any whitespace from ends of string.
        $rec = trim($rec);

        if ($rec != "") 
        {
            $loopcount = 0;
            echo date("d-m-y h:i:sa") . " Got : $rec\n";

	    $data = explode(',', $rec);
            if ($deviceID == 0)
                $deviceID = identify_lommy($data[2]);

	    if ($data[1] == "UNITSTAT")
		skraset($rec, $deviceID);

            if ($data[0] == "-1") // Send config to unit, first time
            {
		send_to_socket($socket, " -1,ACK\r ");

		// Set the lommy to send every 10 seconds
	        send_to_socket($socket, "32,CONF:REPORT-INTERVAL,10\r");
		$unitSerial = $data[1];
	    }
	    elseif($data[0] == "32")
	    {
		echo "Do nothing.\n";
	    }
	    else
		send_to_socket($socket, "$data[0]".",ACK\r");
		


        }
        $rec = "";
    }
}

/**
  * Become a daemon by forking and closing the parent
  */
function become_daemon()
{
    $pid = pcntl_fork();
   
    /* if fork failed */
    if ($pid == -1)
        die("Fork failure!\n");
    elseif ($pid)
        /* close the parent */
        exit();
    else
    {
        /* child becomes our daemon */
        posix_setsid();
        chdir('/');
        umask(0);
        return posix_getpid();

    }
} 
function send_to_socket($socket, $message)
{
    @socket_send($socket, $message, strlen($message)+2, 0);
    echo date("d-m-y h:i:sa") . " Sent: $message\n";
}

function skraset($dataString, $DeviceID)
{
	// Convert to standard'ish data

	//echo '  INSERT INTO "TrackingData" ("DataString") '."\n".' VALUES(\''.$dataString.'\');'."\n";
	

	$data = explode(',',$dataString);

	$latlon = array(
                'latitudeHemisphere' => substr($data[5], 0, 1),
                'latitudeDecimalDegrees' => str_replace(".", "", substr($data[5], 1, 5)).".".str_replace(".", "", substr($data[5], 6)),
                'longitudeHemisphere' => substr($data[6], 0, 1),
                'longitudeDecimalDegrees' => "00".str_replace(".", "", substr($data[6], 1, 4)).".".str_replace(".", "", substr($data[6], 5))
	);
	if (intval($latlon['latitudeDecimalDegrees']) == 0 || intval($latlon['longitudeDecimalDegrees']) == 0)
		return;

	$latlon = to_decimal_degrees($latlon);

	$utm29n = to_utm29n($latlon);

	if ($utm29n['y'] == 0)
		return;

	$dbconn = pg_connect("host=localhost port=5432 dbname=tracking user=postgres");
	if (!$dbconn)
		echo "Ein feilur er hendur, kann ikki knýta til dátugrunn.";

	$query = 'INSERT INTO "TrackingData" ("DataString") VALUES '."('".pg_escape_string($dataString)."')".' RETURNING "ID";';
	$result = pg_query($query);

	if (!$result)
		echo "Ein feilur er hendur, kann ikki seta datasetning inn í dátugrunn.\n";
	$TrackingDataID = intval(current(pg_fetch_row($result)));

	//$DeviceID = 13;

	$query = 'INSERT INTO "Tracking" (
				"TrackingDataID", 
				"DeviceID",
				"Pos",
				"Message",
				the_geom
				) VALUES ( '."
				'".pg_escape_string($TrackingDataID)."',
				'".pg_escape_string($DeviceID)."',
				point '(".$utm29n['x'].", ".$utm29n['y'].")',
				'".pg_escape_string($data[1])."',
				st_setsrid(st_makepoint(".$utm29n['x'].",".$utm29n['y']."), 32629))";

	$result = pg_query($query);
	if (!$result)
		echo "Ein feilur er heindur, kann ikki innseta í tracking talvuna";

	pg_close($dbconn);

}
function identify_lommy($id) {
	$dbconn = pg_connect("host=localhost port=5432 dbname=tracking user=postgres");
	if (!$dbconn)
		echo "Ein feilur er hendur, kann ikki knýta til dátugrunn.";

	$query = 'SELECT "ID" FROM "Device" WHERE "IMEI" = \''.$id.'\'';
	$deviceID = current(pg_fetch_row(pg_query($query)));

	pg_close($dbconn);

	echo "Device identified as device ".$deviceID."\n";
	return $deviceID;
}
?>
