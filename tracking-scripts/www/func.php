<?php

function to_decimal_degrees($data)
{
	strlen($data['latitudeDecimalDegrees']) == 9 && $data['latitudeDecimalDegrees'] = '0'.$data['latitudeDecimalDegrees'];
	$g = substr($data['latitudeDecimalDegrees'],0,3);
	$d = substr($data['latitudeDecimalDegrees'],3);
	$latitudeDecimalDegrees = $g + ($d/60);
	$data['latitudeHemisphere'] == "S" && $latitudeDecimalDegrees = $latitudeDecimalDegrees * -1;

	strlen($data['longitudeDecimalDegrees']) == 9 && $data['longitudeDecimalDegrees'] = '0'.$data['longitudeDecimalDegrees'];
	$g = substr($data['longitudeDecimalDegrees'],0,3);
	$d = substr($data['longitudeDecimalDegrees'],3);
	$longitudeDecimalDegrees = $g + ($d/60);
        $data['longitudeHemisphere'] == "W" && $longitudeDecimalDegrees = $longitudeDecimalDegrees * -1;

	return array(
		'latitude' => $latitudeDecimalDegrees,
		'longitude' => $longitudeDecimalDegrees
	);
}

function to_utm29n($data)
{
	$json = file_get_contents("http://sampleserver1.arcgisonline.com/ArcGIS/rest/services/Geometry/GeometryServer/project?f=json&outSR=32629&inSR=4326&geometries={%22geometryType%22:%22esriGeometryPoint%22,%22geometries%22:[{%22x%22:".$data['longitude'].",%22y%22:".$data['latitude'].",%22spatialReference%22:{%22wkid%22:4326}}]}");
	$arr = json_decode($json,true);
	return array (
		'x' => $arr['geometries'][0]['x'],
		'y' => $arr['geometries'][0]['y']
	);
}
