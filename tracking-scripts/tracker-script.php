#!/usr/bin/php -q
<?php
require_once("www/config.php");
require_once("www/func.php");
/**
  * Listens for requests and forks on each connection
  */

$__server_listening = true;

error_reporting(E_ALL);
set_time_limit(0);
ob_implicit_flush();
declare(ticks = 1); // Needed for pcntl_signal
ini_set('sendmail_from', $from_email);

become_daemon();

/* nobody/nogroup, change to your host's uid/gid of the non-priv user 

** Comment by Andrew - I could not get this to work, i commented it out
   the code still works fine but mine does not run as a priv user anyway....
   uncommented for completeness
*/
change_identity(1001, 1001);

/* handle signals */
pcntl_signal(SIGTERM, 'sig_handler');
pcntl_signal(SIGINT, 'sig_handler');
pcntl_signal(SIGCHLD, 'sig_handler');

/* change this to your own host / port */
server_loop($ip, $port);

/**
  * Change the identity to a non-priv user
  */
function change_identity( $uid, $gid )
{
    if( !posix_setgid( $gid ) )
        die("Unable to setgid to ".$gid."!\n");

    if( !posix_setuid( $uid ) )
        die("Unable to setuid to ".$uid."!\n");
}

/**
  * Creates a server socket and listens for incoming client connections
  * @param string $address The address to listen on
  * @param int $port The port to listen on
  */
function server_loop($address, $port)
{
    GLOBAL $__server_listening;

    if(($sock = socket_create(AF_INET, SOCK_STREAM, 0)) < 0)
        die("Failed to create socket: ".socket_strerror($sock)."\n");

    if(($ret = socket_bind($sock, $address, $port)) < 0)
        die("Failed to bind socket: ".socket_strerror($ret)."!\n");

    if( ( $ret = socket_listen( $sock, 0 ) ) < 0 )
        die("Failed to listen to socket: ".socket_strerror($ret)."!\n");

    socket_set_nonblock($sock);

    echo "waiting for clients to connect\n";

    while ($__server_listening)
    {
        $connection = @socket_accept($sock);

        if ($connection === false)
            usleep(100);

        elseif ($connection > 0)
            handle_client($sock, $connection);

        else
            die("Error: ".socket_strerror($connection));
    }
}

/**
* Signal handler
*/
function sig_handler($sig)
{
    switch($sig)
    {
        case SIGTERM:
        case SIGINT:
            //exit();
            break;

        case SIGCHLD:
            pcntl_waitpid(-1, $status);
            break;
    }
}

/**
* Handle a new client connection
*/
function handle_client($ssock, $csock)
{
    GLOBAL $__server_listening;

    $pid = pcntl_fork();

    /* if fork failed */
    if ($pid == -1)
        die("fork failure!\n");
    elseif ($pid == 0)
    {
        /* child process */
        $__server_listening = false;
        socket_getpeername($csock, $remip, $remport);
        echo date("d-m-y h:i:sa") . " Connection from $remip:$remport\n";
        socket_close($ssock);
        interact($csock);
        socket_close($csock);
        echo date("d-m-y h:i:sa") . " Connection to $remip:$remport closed\n";
    }
    else
        socket_close($csock);
}

function interact($socket)
{
    $loopcount = 0;
    $conn_imei = "354777030482876";
    /* TALK TO YOUR CLIENT */
    $rec = "";
    // Read the socket but don't wait for data..

    $command_path = '/data/gps-tracker/cmds';
    while (@socket_recv($socket, $rec, 2048, 0x40) !== 0) {

        //If we know the imei of the phone and there is a pending command send it.
        if ($conn_imei != "" and file_exists("$command_path/$conn_imei"))
        {
            $send_cmd = file_get_contents("$command_path/$conn_imei");
            socket_send($socket, $send_cmd, strlen($send_cmd), 0);
            echo date("d-m-y h:i:sa") . " Sent: $send_cmd\n";
            unlink("$command_path/$conn_imei");
        }

        // Some pacing to ensure we don't split any incoming data.
        sleep (1);

        // Timeout the socket if it's not talking...
        // Prevents duplicate connections, confusing the send commands
        $loopcount++;
        if ($loopcount > 120) return;

        // remove any whitespace from ends of string.
        $rec = trim($rec);

        if ($rec != "") 
        {
            $loopcount = 0;
            echo date("d-m-y h:i:sa") . " Got : $rec\n";
            $parts = explode(',',$rec);
            if (strpos($parts[0], "#") === FALSE )
            {
        global $db_host, $db_username, $db_password;
                $cnx = mysql_connect($db_host, $db_username, $db_password);
                /* Andrew's tracker is different....
           Array
            (       
            [0] => imei:354779030525274
                [1] => tracker
                [2] => 0909221022
                [3] => <admin ph no>
                [4] => F
                [5] => 022234.000
                [6] => A
                [7] => xxxx.xxxx
                [8] => S
                [9] => xxxxx.xxxx
                [10] => E
                [11] => 0.00
                [12] => 
            )
            */
            // $imei                       = substr($parts[0],0, -1);
        
            // Only worry about the rest if there is data to get
                if (count($parts) > 1) 
                {
                    $imei                = substr($parts[0],5);
                    $infotext                = mysql_real_escape_string($parts[1]);
                    $trackerdate                = mysql_real_escape_string($parts[2]);
                    $phone                      = mysql_real_escape_string($parts[3]);
                    $gpsSignalIndicator         = mysql_real_escape_string($parts[4]);
                    $satelliteFixStatus         = mysql_real_escape_string($parts[6]);
                    $latitudeDecimalDegrees     = mysql_real_escape_string($parts[7]);
                    $latitudeHemisphere         = mysql_real_escape_string($parts[8]);
                    $longitudeDecimalDegrees    = mysql_real_escape_string($parts[9]);
                    $longitudeHemisphere        = mysql_real_escape_string($parts[10]);
                    $speed                      = mysql_real_escape_string($parts[11]);
      

                 $utm29n = to_utm29n(to_decimal_degrees(array(
            'latitudeDecimalDegrees' => $latitudeDecimalDegrees,
            'latitudeHemisphere' => $latitudeHemisphere,
            'longitudeDecimalDegrees' => $longitudeDecimalDegrees,
            'longitudeHemisphere' => $longitudeHemisphere
            )));

                  // Write it to the database...
                  mysql_select_db('tracker', $cnx);
                  if($gpsSignalIndicator != 'L')
            {
                          mysql_query("INSERT INTO gprmc 
                   (date, 
                imei, 
                phone, 
                satelliteFixStatus, 
                latitudeDecimalDegrees, 
                latitudeHemisphere, 
                longitudeDecimalDegrees, 
                longitudeHemisphere, 
                speed, 
                infotext, 
                gpsSignalIndicator, 
                x, 
                y) 

                 VALUES (
                now(), 
                '$imei', 
                '$phone', 
                '$satelliteFixStatus', 
                '$latitudeDecimalDegrees', 
                '$latitudeHemisphere', 
                '$longitudeDecimalDegrees', 
                '$longitudeHemisphere', 
                '$speed', 
                '$infotext', 
                '$gpsSignalIndicator', 
                '".$utm29n['x']."', 
                '".$utm29n['y']."
            ')", $cnx);

            $dbconn = pg_connect("host=localhost port=5432 dbname=tracking user=postgres");
            if (!$dbconn)
                echo "Ein feilur er hendur, kann ikki knýta til dátugrunn.";
            $query = 'INSERT INTO "TrackingData" ("DataString") VALUES '."('".pg_escape_string($rec)."')".' RETURNING "ID";';
            $result = pg_query($query);

            if (!$result)
                echo "Ein feilur er hendur, kann ikki seta datasetning inn í dátugrunn.\n";
            $row = pg_fetch_row($result);
            $TrackingDataID = $row[0];

            $DeviceID = finn_device_id("IMEI", $imei);

            $query = 'INSERT INTO "Tracking" (
                        "TrackingDataID", 
                        "DeviceID",
                        "Pos",
                        "Message",
                        the_geom
                    ) VALUES ( '."
                        '".pg_escape_string($TrackingDataID)."',
                        '".pg_escape_string($DeviceID)."',
                        point '(".$utm29n['x'].", ".$utm29n['y'].")',
                        '".pg_escape_string($infotext)."',
                        ST_SetSRID(
                            ST_MakePoint(".$utm29n['x'].",".$utm29n['y']."),
                            32629
                        )    
                    )";

            $result = pg_query($query);
            if (!$result)
                echo "Ein feilur er heindur, kann ikki innseta í tracking talvuna";
            pg_close($dbconn);

            
            }
                 // Now check to see if we need to send any alerts.
                 if ($infotext != "tracker")
            {
                       $res = mysql_query("SELECT * FROM alerts WHERE imei='$imei'", $cnx);
                       while($data = mysql_fetch_assoc($res))
                {
                       switch ($infotext){
                      case "dt":
                         $body = "Disable Track OK";
                        break;
                      case "et":
                           $body = "Stop Alarm OK";
                        break;
                      case "gt";
                            $body = "Move Alarm set OK";
                         break;
                         case "help me":
                         $body = "Help!";
                          break;
                      case "ht":
                           $body = "Speed alarm set OK";
                         break;
                      case "it":
                         $body = "Timezone set OK";
                          break;
                      case "low battery":
                         $body = "Low battery!\nYou have about 2 minutes...";
                         break;
                      case "move":
                         $body = "Move Alarm!";
                            break;
                      case "nt":
                            $body = "Returned to SMS mode OK";
                          break;
                      case "speed":
                         $body = "Speed alarm!";
                         break;
                      case "stockade":
                         $body = "Geofence Violation!";
                         break;
                      }
                 $headers = "From: $email_from"."\r\n"."Reply-To: $email_from" . "\r\n";
                     $responsible = $data['responsible'];
                     $rv = mail($responsible, "Tracker - $imei", $body, $headers);

                       } 
                 }
                  mysql_close($cnx);
                  }
                else
                 {
         /* If we got here, we got an imei ONLY - not even 'imei:' first
        This seems to be some sort of 'keepalive' packet
            The TK-201 is not stateless like the TK-102, it
        needs to retain a session.  Basically, we just reply with 'ON'
        anything else seems to cause the device to reset the connection.
         */
            @socket_send($socket, "ON", 2, 0);
            echo date("d-m-y h:i:sa") . " Sent: ON\n";
                 }
        }
        else
        {
          /* Here is where we land on the first iteration of the loop
        on a new connection. We get from the gps: ##,imei:<IMEI>,A;
          It seems the correct reply is 'LOAD' so that's what we send.
          */
            $init = $parts[0];
            $conn_imei = substr($parts[1],5);
            $cmd = $parts[2];
            if ($cmd = "A")
            {
            @socket_send($socket, "LOAD", 4, 0);
            echo date("d-m-y h:i:sa") . " Sent: LOAD\n";
            }
        }
        }
        $rec = "";
    }
}

/**
  * Become a daemon by forking and closing the parent
  */
function become_daemon()
{
    $pid = pcntl_fork();
   
    /* if fork failed */
    if ($pid == -1)
        die("Fork failure!\n");
    elseif ($pid)
        /* close the parent */
        exit();
    else
    {
        /* child becomes our daemon */
        posix_setsid();
        chdir('/');
        umask(0);
        return posix_getpid();

    }
} 

function finn_device_id($mode, $id)
{
    $id = pg_escape_string($id);
    switch($mode)
    {
        case "IMEI":
            $dbconn = 
            $result = pg_query('SELECT "ID" FROM "Device" WHERE "IMEI" = '."'".$id."'");
            if (!$result)
                echo "Kann ikki finna IMEI, skráset trackaran!";
            else
            {
                $row = pg_fetch_row($result);
                return $row[0];
            }
            
            break;
        default: 
            return 0;
            break;
    }
}

?>
