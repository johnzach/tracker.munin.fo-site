<header class="jumbotron subhead" id="umsiting">
    <h1>Umsiting</h1>
</header>

<section id="stevnur">
    <div class="page-header">
        <h1>Stevnur</h2>
    </div>

    <div class="row">
        <span class="span8">
            <table class="table table-striped table-bordered">
            <tr>
                <th>Navn</th>
                <th>map_area</th>
            </tr>
            <?php foreach($stevnur as $stevna): ?> 
                <tr>
                    <td><?php echo $stevna->navn; ?></td>
                    <td><?php echo $stevna->map_area; ?></td>
                </tr>
            <?php endforeach; ?>
            </table>
        </span>
        <span class="span4">
            <a class="btn" href="<?php echo site_url('umsiting/stovnaStevnu'); ?>">Legg stevnu afturat</a>
        </span>
    </div>
</section>

<section id="rodrar">
    <div class="page-header">
        <h1>Róðrar</h2>
    </div>

    <div class="row">
        <span class="span8">
            <table class="table table-striped table-bordered">
            <tr>
                <th>Stevna</th>
                <th>Heiti</th>
            </tr>
            <?php foreach($rodrar as $rodur): ?> 
                <tr data-rowlink="<?php echo site_url(array('kort', 'stevna', $rodur->stevna, 'Róður', $rodur->navn)); ?>">
                    <td><?php echo $rodur->stevna; ?></td>
                    <td><?php echo $rodur->navn; ?></td>
                </tr>
            <?php endforeach; ?>
            </table>
        </span>
        <span class="span4">
            <a class="btn" href="<?php echo site_url('umsiting/stovnaStevnu'); ?>">Legg róður afturat</a>
        </span>
    </div>
</section>
<pre class="prettyprint linenums">
<?php print_r($rodrar); ?>
</pre>
