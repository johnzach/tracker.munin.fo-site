<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="initial-scale=1, maximum-scale=1,user-scalable=no"/>
    <title>GPS-Tracking</title>
    <link rel="stylesheet" href="http://serverapi.arcgisonline.com/jsapi/arcgis/2.8/js/dojo/dijit/themes/tundra/tundra.css" />
    <link rel="stylesheet" href="/tracking/style_new.css" media="all" />
    <script>
        var dojoConfig = { parseOnLoad: true }; 
    </script>
    <script src="http://serverapi.arcgisonline.com/jsapi/arcgis/?v=2.8"></script>
    <script src="/assets/js/dojo-extend.js"></script>
    <script>
        // MuninTrackerConfig
        var mtConfig = {
            refreshRate: 1500,
            maps: [
                new esri.layers.ArcGISTiledMapServiceLayer("http://ags10.kort.fo/ArcGIS/rest/services/munin/Muninkort-vegir2/MapServer"),
                new esri.layers.ArcGISDynamicMapServiceLayer("http://ags10.kort.fo/ArcGIS/rest/services/rsf/rodraroki/MapServer")
            ],
            initialExtent: new esri.geometry.Extent({
                xmax: 609996,
                xmin: 607287,
                ymax: 6888065,
                ymin: 6887161,
                "spatialReference": {
                    "wkid": 32629
                }
            }),
            lineService: 'http://tracker.munin.fo/api/rsf/<?=$url['stevna']?>/<?=$url['R%C3%B3%C3%B0ur']?>',
            syncLines: true,
            EXTRAPOLATE_SPEED_POINTS: 3
        };
    </script>
    <!--[if IE]>
    <style type="text/css">
        #admin {
            background:transparent;
            filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#cc646464,endColorstr=#cc646464);
            zoom: 1;
            width: 212px;
        } 
    </style>
    <![endif]-->
    <script src="/assets/js/rsf_kort.js"></script>
  </head>

  <body class="tundra">
    <div data-dojo-type="dijit.layout.BorderContainer" data-dojo-props="design:'headline', gutters:false" style="width: 100%; height: 100%; margin: 0;">
      <div id="map" data-dojo-type="dijit.layout.ContentPane" data-dojo-props="region:'center'">
        <div id="admin">
        </div>
      </div>
    </div>
    <div id="copyright"><a href="http://munin.fo" target="_blank" title="Vitja munin.fo">&copy; Munin 2012</a></div>
  </body>
</html>


