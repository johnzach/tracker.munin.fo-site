<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Umsiting extends CI_Controller {
    public $data;
    function __construct() {
        parent::__construct();
        $this->load->model('stevnumodel');
        $this->load->model('rodrarmodel');
        $this->data = array();
    }

	public function index() {
        $this->data['stevnur'] = $this->stevnumodel->getStevnur();
        $this->data['rodrar'] = $this->rodrarmodel->getRodrar();
        $this->data['title'] = "Umsiting";

        $this->load->view('head', $this->data);
        $this->load->view('umsiting', $this->data);
        $this->load->view('foot');
	}

    public function stovnaStevnu() {
        $this->load->view('head');
        $this->load->view('stevnur/stovna', $this->data);
        $this->load->view('foot');
        

    }
}

