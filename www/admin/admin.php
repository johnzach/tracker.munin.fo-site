<?php
ini_set("DISPLAY_ERRORS", 1);
error_reporting(E_ALL);

require_once("func.php");
$session = new Session();

if (isset($_COOKIE['logged_in']) && !isset($_SESSION['email'])) {
    $_SESSION['email'] = $_COOKIE['logged_in'];
    setcookie('logged_in', $_COOKIE['logged_in'], time()+60*60*24*14);
}


if (isset($_SESSION['email']))
	switch ($_GET['action'])
	{
		case "manage_trackers":
			manage_trackers();
			break;
		case "add_unit":
			add_unit();
			break;
		case "logout":
			logout();
            $session->delete();
			break;
		case "createuser":
			create_user();
			break;
        case "administer_layers":
            administer_layers($_SESSION['email']);
            break;
		default:
			greet_user($_SESSION['email']);
			break;
	}

if (empty($_POST['email']) && empty($_POST['password'])) {
	die(include ("loginform.php"));
}
else {
        $user = array(
                'email' => $_POST['email'],
                'password' => $_POST['password']
        );

        // Check for validity
        authenticate_user($user);

        if (isset($_GET['redirect'])) {
            header("Location: //" . $_SERVER['HTTP_HOST']. $_GET['redirect']);
        } else {
            header("Location: //" . $_SERVER['HTTP_HOST']. "/");
        }

}
