<?php
  class Session {
    private $alive = true;
    private $dbc = NULL;
   
    function __construct() {
      session_set_save_handler(
        array(&$this, 'open'),
        array(&$this, 'close'),
        array(&$this, 'read'),
        array(&$this, 'write'),
        array(&$this, 'destroy'),
        array(&$this, 'clean'));
   
      session_start();
    }
   
    function __destruct() {
      if($this->alive) {
        session_write_close();
        $this->alive = false;
      }
    }
   
    function delete() {
      if(ini_get('session.use_cookies')) {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000,
          $params['path'], $params['domain'],
          $params['secure'], $params['httponly']
        );
      }
     
      session_destroy();
     
      $this->alive = false;
    }
     
    private function open() {    
      return true;
    }
     
    private function close() {
      return true;
    }
     
    private function read($sid) {
      $q = "SELECT data FROM sessions WHERE id = '".pg_escape_string($sid)."' LIMIT 1";
      $r = pg_query($q);
     
      if(pg_num_rows($r) == 1) {
        $fields = pg_fetch_assoc($r);
        return $fields['data'];
      }
      else {
        return '';
      }
    }
     
    private function write($sid, $data) {
      $q = "SELECT * FROM sessions WHERE id = '".pg_escape_string($sid)."'";
      $r = pg_query($q);

      if (pg_num_rows($r) > 0 ) {
        $q = "UPDATE sessions SET data = '".pg_escape_string($data)."', last_accessed=NOW() WHERE id = '".pg_escape_string($sid)."'";
      }
      else {
        $q = "INSERT INTO sessions (id, data, ip) VALUES ('".pg_escape_string($sid)."','".pg_escape_string($data)."', '".$_SERVER['REMOTE_ADDR']."')";
      }
     
      $r = pg_query($q);

      if (!$r) {
        die(pg_last_error());
      }

      return pg_affected_rows($r);
    }
   
    private function destroy($sid) {
      $q = "DELETE FROM `sessions` WHERE `id` = '".pg_escape_string($sid)."'"; 
      $r = pg_query($q);
   
      $_SESSION = array();
   
      return pg_affected_rows($r);
    }
   
    private function clean($expire) {
      $q = "DELETE FROM `sessions` WHERE DATE_ADD(`last_accessed`, INTERVAL ".(int) $expire." SECOND) < NOW()"; 
      $r = pg_query($q);
   
      return pg_affected_rows($r);
    }

  }
