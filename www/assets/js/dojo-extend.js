dojo.Color.prototype.setOpacity = function (opacity) {
    "use strict";
    if (!opacity) {
        opacity = 1;
    }
    var color = this.toRgb();
    color.push(opacity);
    this.setColor(color);
};
dojo.Color.prototype.opacity = function (opacity) {
    "use strict";
    if (!opacity) {
        return this;
    }
    var color = this.toRgb();
    color.push(opacity);
    color = this.setColor(color);
    return color;
};
