var mtData;
(function () {
    "use strict";

    /* JS Lint */
    // var dojo, dijit, clearTimeout, esri, window, map, resizeTimer, mtConfig, mtData;
    dojo.require("dijit.layout.BorderContainer");
    dojo.require("dijit.layout.ContentPane");
    dojo.require("esri.map");
    dojo.require("esri.symbol");

    // I MOVED MTDATA TO GLOBAL SCOPE WHILST DEVELOPING
    var map, resizeTimer, hasRun = false;

    function Line(points) {
        this.points = points;
        this.a = (points[0].y - points[1].y) / (points[0].x - points[1].x);
        this.b = points[0].y - this.a * points[0].x;
        // y = ax + b
        // this.points[0].y = this.a * this.points[0].x + b
    }

    function getSyncLine(batur) {
        var V1 = new Line([
                {x: 607740.5149, y: 6887136.8002},
                {x: 609601.5527, y: 6887867.8441}
            ]),
            V2 = new Line([
                {x: 607824.2412, y: 6886923.6549},
                {x: 609685.279, y: 6887654.6988}
            ]),
            startur = new Line([
                {x: 608740.1348, y: 6887326.4065},
                {x: 608683.8296, y: 6887469.7443}
            ]),
            Xi = (V1.b - (batur.y - startur.a * batur.x)) / (startur.a - V1.a),
            Yi = (startur.a * Xi + (batur.y - startur.a * batur.x)),
            result = [{x: Xi, y: Yi}],
            pl = new esri.geometry.Polyline(map.spatialReference);

        Xi = (V2.b - (batur.y - startur.a * batur.x)) / (startur.a - V2.a);
        Yi = (startur.a * Xi + (batur.y - startur.a * batur.x));
        result.push({x: Xi, y: Yi});

        pl.addPath(result);
        return pl;
    }

    Array.prototype.getDevice = function (device_id) {
        var i;
        for (i = 0; i < this.length; i = i + 1) {
            if (this[i].hasOwnProperty("device_id") && this[i].device_id === device_id) {
                return this[i];
            }
        }
        return false;
    };

    mtData = {
        devices: [],
        graphics: new esri.layers.GraphicsLayer(),
        syncGraphic: new esri.layers.GraphicsLayer(),
        kapprodur: {
        }
    };

    // Draw a route for every registered device
    function drawRoute(device) {
        var pointGraphic, pointNameGraphic, point, outline, symbol, p;
        p = device.getExtrapolatedPoint();

        point = new esri.geometry.Point(p.x, p.y, map.spatialReference);
        outline = new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([0, 0, 0]), 1);
        if (p.time < 5) {
            symbol = new esri.symbol.SimpleMarkerSymbol(esri.symbol.SimpleMarkerSymbol.STYLE_DIAMOND, 20, outline, device.color.opacity(0.6));
        } else {
            symbol = new esri.symbol.SimpleMarkerSymbol(esri.symbol.SimpleMarkerSymbol.STYLE_DIAMOND, 20, outline, new dojo.Color([150, 150, 150, 0.6]));
        }

        pointGraphic = new esri.Graphic(point, symbol);

        symbol = new esri.symbol.TextSymbol(device.navn).setOffset(10, 15).setAngle(device.getAngle());
        if (device.hasOwnProperty('speed') && device.speed !== null) {
            symbol = new esri.symbol.TextSymbol(device.navn + ", " + Math.round(device.speed * 36) / 10 + "km/t").setOffset(10, 15);
        }

        pointNameGraphic = new esri.Graphic(point, symbol);

        mtData.graphics.add(pointGraphic);
        mtData.graphics.add(pointNameGraphic);
    }

    function centerOnTracker(device) {
        map.centerAt(
            new esri.geometry.Point(device.getPoint().x, device.getPoint().y, map.spatialReference)
        );
    }

    function displayDevices() {
        var container = dojo.byId("admin"), position = 0;

        while (container.hasChildNodes()) {
            container.removeChild(container.lastChild);
        }
        /**
         * Sort the mtData.devices array according to the current score. 
         **/
        mtData.devices.sort(function (a, b) {
            if (a.hasOwnProperty('distanceFromStart') && b.hasOwnProperty('distanceFromStart')) {
                return a.distanceFromStart() >= b.distanceFromStart();
            }
            return 0;
        });

        dojo.forEach(mtData.devices, function (device) {
            var deviceInfo, name, nameContent, timespan, time;
            position += 1;
            if (position === 1) {
                mtData.syncGraphic.clear();
                mtData.syncGraphic.add(new esri.Graphic(getSyncLine(device), new esri.symbol.SimpleLineSymbol()));
            }

            deviceInfo = dojo.doc.createElement("div");
            dojo.addClass(deviceInfo, "unit");

            name = dojo.doc.createElement("div");
            //name.style.color = device.color.opacity(0.6);
            name.style.padding = "3px 10px";
            name.style.fontFamily = "Helvetica";
            name.style.cursor = 'pointer';
            nameContent = dojo.doc.createTextNode(position + ". " + device.navn);

            name.appendChild(nameContent);

            if (device.datetime !== undefined) {
                timespan = dojo.doc.createElement("span");
                timespan.setAttribute("id", "time" + device.device_id);
                time = dojo.doc.createTextNode(", " + device.datetime);
                timespan.appendChild(time);
                name.appendChild(timespan);
            }

            deviceInfo.appendChild(name);

            dojo.connect(name, "onclick", function () {
                centerOnTracker(device);
            });

            dojo.place(deviceInfo, container, "last");
        });

    } // End function displayDevices

    /**
     * Ikki staðsettir vektorar
     **/
    function Vector(a, b) {
        this.x = (b.x - a.x);
        this.y = (b.y - a.y);

        this.scale = function (n) {
            return new Vector({x: 0, y: 0}, {
                x: this.x * n,
                y: this.y * n
            });
        };

        this.length = function () {
            return Math.sqrt(
                Math.pow(this.x, 2) +
                    Math.pow(this.y, 2)
            );
        };
    }

    function Point(point) {
        this.x = point.x;
        this.y = point.y;
        this.distanceTo = function (point) {
            /**
             * This function will return the distance to another point. 
             * Always a positive double.
             **/
            return Math.sqrt(
                Math.pow((this.x - point.x), 2) +
                    Math.pow((this.y - point.y), 2)
            );

        };
        this.hashCode = function () {
            return this.x + "#" + this.y + "#" + this.timestamp;
        };
        this.distanceToLine = function (line) {
            /**
             * Based on 
             * http://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line#Cartesian_coordinates
             * 
             * First convert the line into general linear form
             * http://bobobobo.wordpress.com/2008/01/07/solving-linear-equations-ax-by-c-0/
             **/
            var a = line.points[0].y - line.points[1].y,
                b = line.points[1].x - line.points[0].x,
                c = line.points[0].x * line.points[1].y - line.points[1].x * line.points[0].y;
                    
            // First of all, convert the into a linear equation ax + by + c = 0
            return (Math.abs(a * this.x + b * this.y + c) /
                    Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2)));
        };
        this.timestamp = point.timestamp;
        if (point.hasOwnProperty('time')) { this.time = point.time; }
    }
    function Device(device) {
        this.points = [];
        this.device_id  = device.device_id;
        this.getPoint = this.getLastPoint = function () {
            return (this.points[this.points.length - 1]);
        };
        this.getFirstPoint = function () {
            return (this.points[0]);
        };
        this.findPoint = function (hashCode) {
            var i;
            for (i = 0; i < this.points.length; i = i + 1) {
                if (this.points[i].hasOwnProperty('hashCode') && this.points[i].hashCode() === hashCode) {
                    return i;
                }
            }
            return -1;
        };
        this.addPoint = function (point) {
            if (point instanceof Point) {
                /**
                 * Lookup based on hashes
                 **/
                if (this.findPoint(point.hashCode()) === -1) {
                    this.points.push(point);
                }
            }
        };
        this.getExtrapolatedPoint = function () {
            if (this.points.length < (mtConfig.EXTRAPOLATE_SPEED_POINTS + 2)) {
                return this.getPoint();
            }

            var time = 0, timedifference, extraVector,
                p = this.points[this.points.length - (mtConfig.EXTRAPOLATE_SPEED_POINTS + 1)],
                directionVector = new Vector(this.getFirstPoint(), this.getLastPoint()),
                speedVector = new Vector(p, this.getLastPoint());

            dojo.forEach(mtData.devices, function (d) {
                if (d.getPoint().timestamp > time) { time = d.getPoint().timestamp; }
            });

            timedifference = time - this.getLastPoint().timestamp;

            extraVector = directionVector.scale(
                (speedVector.length() * (timedifference / (this.getLastPoint().timestamp - p.timestamp))) / directionVector.length()
            );

            return new Point({
                x: this.getPoint().x + extraVector.x,
                y: this.getPoint().y + extraVector.y,
                time: (timedifference / 1000)
            });
        };
        this.getAngle = function () {
            var angle = Math.abs((Math.PI * 180 / Math.atan2(
                (this.getLastPoint().y - this.getFirstPoint().y),
                (this.getLastPoint().x - this.getFirstPoint().x)
            )) + 90) % 180;
            return 0;
            // return angle;
        };
        this.distanceFromStart = function () {
            /**
             * Returns distance in meters 
             **/
            if (mtData.kapprodur.startur instanceof Line) {
                return this.getExtrapolatedPoint().distanceToLine(mtData.kapprodur.startur);
            }
            return;
        };
        if (device.hasOwnProperty('navn')) { this.navn = device.navn; }
        if (device.hasOwnProperty('rgb')) { this.color = new dojo.Color(device.rgb); }
        if (device.hasOwnProperty('kvf')) { this.kvf = device.kvf; }
    }
    function get_devices() {
        dojo.xhrGet({
            url: mtConfig.lineService,
            timeout: 30000,
            handleAs: 'json',
            load: function (result) {
                dojo.forEach(result, function (device) {
                    if (mtData.devices.getDevice(device.device_id) === false) {
                        mtData.devices.push(new Device(device));
                    }

                    mtData.devices.getDevice(device.device_id).addPoint(new Point({
                        x: device.x,
                        y: device.y,
                        timestamp: new Date(device.timestamp)
                    }));
                });

                mtData.graphics.clear();
                dojo.forEach(mtData.devices, function (device) {
                    drawRoute(device);
                });
                displayDevices();
            }
        });
        window.setTimeout(get_devices, mtConfig.refreshRate);
    }


    function init() {
        get_devices();
        map = new esri.Map("map", { extent: mtConfig.initialExtent, logo: false});

        if (mtConfig.hasOwnProperty('stevna')) {
            mtData.kapprodur.stevna = mtConfig.stevna;
        }

        dojo.forEach(mtConfig.maps, function (layer) {
            map.addLayer(layer);
        });
        map.addLayer(mtData.graphics);
        map.addLayer(mtData.syncGraphic);

        dojo.connect(window, 'onresize', function () {
            window.clearTimeout(resizeTimer);
            resizeTimer = window.setTimeout(function () {
                map.resize();
            }, 200);
        });
    } // End function init

    dojo.ready(init);
}());

var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-26546469-1']);
_gaq.push(['_setSiteSpeedSampleRate', 50]);
_gaq.push(['_trackPageview']);

(function () {
    'use strict';
    var ga, s;
    ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = ('https:' === document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ga, s);
}());
