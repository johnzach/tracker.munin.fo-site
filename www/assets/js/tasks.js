/*global mtData, console, $, esri, dojo, document, tasks, escape, require */
/*jslint bitwise: true */
function Tasks() {
    "use strict";
    var self = this;
    this.data = {
        tasks: [],
        devices: [],
        statuses: []
    };

    this.devices = {
        find: function (device_id) {
            // This function assumes data.devices is set
            var result;
            $(self.data.devices).each(function (i, device) {
                if (device.id === device_id) {
                    result = device;
                }
            });
            return result;
        },
        get: function (callback) {
            $.ajax({
                url: '/api/devices.json',
                data: {
                    token: self.token.token
                },
                success: function (data) {
                    callback(data);
                }
            });
        }
    };

    this.errorHandler = function (event, error) {
        console.log(error);
        if (error.error.status === 401) {
            self.token.get(function (token) {
                console.log(token);
            });
        }
    };

    this.get = function (callback) {
        $.ajax({
            url: '/api/tasks.json',
            data: {
                token: self.token.token,
                fields: 'location'
            },
            success: function (result) {
                callback(result.tasks);
            },
            error: function (error) {
                $(self).trigger('task_error', {
                    error: error,
                    method: 'tasks.get',
                    message: 'Kundi ikki heinta uppgávur'
                });
            }
        });
    };

    /**
     * Updates a task to new data.
     * Used when a new task is received from socket.io
     *
     * Will first go over the list of tasks, and match by ID. If a match is found, then the attributes will be transferred over.
     * If not, getTask will be called.
     */
    this.update = function (updated_task, cb) {
        // Iterate over list of tasks
        self.data.tasks.forEach(function (task) {
            if (task.equalsById(updated_task)) {
                task.merge(updated_task, cb);
            }
        });

    };

    this.getStatuses = function (callback) {
        $.ajax({
            url: '/api/tasks/statuses.json',
            data: {
                token: self.token.token
            },
            success: function (result) {
                callback(result);
            }
        });
    };

    /**
     * Used to get a task from server
     */
    this.getTask = function (callback, id) {
        $.ajax({
            url: '/api/tasks/' + id + '.json',
            data: {
                token: self.token.token,
                fields: 'locations'
            },
            success: function (result) {
                require(['/assets/js/Task.js'], function (Task) {
                    callback(new Task(result));
                });
            },
            error: function (error) {
                $(self).trigger('task_error', {
                    error: error,
                    method: 'tasks.getTask',
                    message: 'Kundi ikki heinta uppgávu #' + id
                });
            }
        });

    };

    this.deleteTask = function (task, callback) {
        console.log("Given orders to delete task: ", task);
        if (task.task.id) {
            this.data.tasks = this.data.tasks.filter(function (filter_task) {
                console.log("Filtering task ", filter_task.task.description, "out: ", (task.task.id !== filter_task.task.id));
                return (task.task.id !== filter_task.task.id);
            });
            $.ajax({
                url: '/api/tasks/' + task.task.id + '?token=' + escape(self.token.token),
                type: 'DELETE',
                success: callback,
                error: callback
            });
        }
    };

    this.init = function () {
        $(self).bind('task_error', this.errorHandler);
        this.token.init();

        $(document).one('task_token', function () {
            console.log('task_token called!');

            self.getStatuses(function (statuses) {
                self.data.statuses = statuses;
            });

            self.get(function (tasks) {
                $(tasks).each(function (i, task) {
                    self.getTask(function (task) {
                        self.data.tasks.push(task);
                        if (i === tasks.length - 1) {
                            self.UI.fillTaskOverview();
                            $(document).trigger('tasks_ready');
                        }
                    }, task.id);
                });
                if (tasks.length === 0) {
                    // Trigger tasks ready, even if there are no tasks
                    $(document).trigger('tasks_ready');
                }
            });
            self.devices.get(function (devices) {
                self.data.devices = devices.devices;
            });
        });
    };
    this.UI = {
        generateCheckboxes: function (data, name, htmlElement) {
            $(data).each(function (index, element) {
                $(htmlElement).prepend('<span><input type="checkbox" name="' + name +
                    '" value="' + element.id + '">' + element.name + '</span>');
            });
        },
        fillTaskOverview: function () {
            var taskOverview;
            $(self.data.tasks).each(function (i, element) {
                taskOverview = '<div class="task"><span class="taskDescription">' + element.task.description + '</span>';

                taskOverview += '<div class="devices"><h4>Eind:</h4>';
                $(element.task.devices).each(function (i, device) {
                    if (self.devices.find(device.id) !== undefined) {
                        taskOverview += '<div class="device">' + self.devices.find(device.id).name + '</div>';
                    }
                });
                taskOverview += '</div>';

                taskOverview += '<div class="statuses">';
                $(element.task.statuses).each(function (i, statusObj) {
                    taskOverview +=  '<div class="status"> ' +
                        '<div class="description">' + statusObj.description + '</div>' +
                        '<div class="datetime">' + new Date(statusObj.datetime) + '</div>' +
                        '</div>';
                });
                taskOverview += '</div>';


                taskOverview += '</div>';

                $('.task_overview .tasks').prepend(taskOverview);
            });
        }
    };
    this.reload = function (mtData, callback) {
        // This function reloads the tasks from the API, and refreshes accordingly on screen
        var newTasks = [];
        this.get(function (tasks) {
            $(tasks).each(function (i, task) {
                self.getTask(function (task) {
                    newTasks.push(task);
                    if (i === tasks.length - 1) {
                        self.data.tasks = newTasks;
                        self.show();
                        callback();
                    }
                }, task.id);
            });
        });
    };

    /**
     * This will display all the tasks
     */
    this.show = function () {
        $(this.data.tasks).each(function (i, task) {
            // This should be implemented as a method in Task class
            // Function definition:
            // create dom elements for taskElement, description, a list to hold statuses, a date and a button
            //
            // append values from task object, status list, date etc.
            // then we bind two click handlers
            task.show();


        });
    };
}
