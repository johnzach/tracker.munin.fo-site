/*global define*/
function TaskStatus(state) {
    "use strict";
    this.system_status = state.system_status;
    this.description = state.description;
    this.datetime = new Date(state.datetime);
    this.getDescription = function () {
        return (this.datetime.toDDMM() + ', ' + this.datetime.toHHMM() + ' ' + this.description);
    };
}

define(['/assets/js/TaskStatus.js'], function () {
    "use strict";
    return TaskStatus;
});
