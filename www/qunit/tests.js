
test("Date display", function () {
    "use strict";
    equal(new Date("2008/01/28 22:24:30").toDDMM(), "28/01");
    equal(new Date("2013/01/01 22:24:30").toDDMM(), "01/01");
    equal(new Date("2013/12/12 22:24:30").toDDMM(), "12/12");

    equal(new Date("2008/01/28 00:00:01").toHHMM(), "00:00");
    equal(new Date("2013/01/01 22:24:30").toHHMM(), "22:24");
    equal(new Date("2013/12/12 01:05:00").toHHMM(), "01:05");

});
