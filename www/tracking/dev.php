<?php   
        require_once("../admin/func.php");
        $session = new Session();
        require_once("../tracking/public_access.php");
        if (empty($_SESSION['email']) && empty($publickey)) // I should implement at least two modes. Embedded and full browser.
            header("Location: /admin/admin.php");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=9">

    <title>GPS-Tracking</title>
    <link rel="stylesheet" href="//js.arcgis.com/3.7/js/esri/css/esri.css">
    <link rel="stylesheet" href="//js.arcgis.com/3.7/js/dojo/dijit/themes/tundra/tundra.css">
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet/less" type="text/css" href="/tracking/style_new.css.less" media="all" />
    <script src="/assets/js/less-1.4.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/tracking/phone.css"></script>

    <script data-dojo-config="async:true" src="//js.arcgis.com/3.7/"></script>
    <script>
        // MuninTrackerConfig
        var mtConfig = {
            refreshRate: 10000,
            showTimeOnPoint: true,
            maps: [
                {type: 'ArcGISTiledMapServiceLayer', url: "//srv8.kort.fo/arcgis/rest/services/munin/muninkort_utm29/MapServer"},
                {type: 'ArcGISDynamicMapServiceLayer', url: "//srv8.kort.fo/arcgis/rest/services/munin/us_adressur/MapServer"}
            ],
            initialExtent: {
            <?php if (isset($_SESSION['xmin']) && isset($_SESSION['xmax']) && isset($_SESSION['ymin']) && isset($_SESSION['ymax'])): ?>
                "xmin": <?=$_SESSION['xmin']?>, 
                "ymin": <?=$_SESSION['ymin']?>, 
                "xmax": <?=$_SESSION['xmax']?>, 
                "ymax": <?=$_SESSION['ymax']?>, 
            <?php else: ?> 
                "xmin": 584390, 
                "ymin": 6868745, 
                "xmax": 638036, 
                "ymax": 6891755, 
            <?php endif; ?>
                "spatialReference": {"wkid": 32629}
            },
            slider: {
                minValue: 2,
                maxValue: 1000,
                enabled: true
            },
            addressSearch: true
        };
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="../assets/js/jquery.titlealert.js"></script>
    <script src="../assets/js/handlebars-v1.3.0.js"></script>
    <script src="../assets/js/js-extend.js"></script>
    <script src="../assets/js/bootstrap-typeahead.js"></script>
    <script src="../assets/js/bootstrap-modal.js"></script>
    <script src="../assets/js/bootstrap-dropdown.js"></script>
    <script src="js-dev.js"></script>
    <script src="tasks-ui.js"></script>
    <script src="ga.js"></script>
  </head>

  <body class="tundra">
    <div 
        data-dojo-type="dijit.layout.BorderContainer"
        data-dojo-props="design:'headline',gutters:false"
        style="width: 100%; height: 100%; margin: 0;">
      <div id="map"
          data-dojo-type="dijit.layout.ContentPane"
          data-dojo-props="region:'center'">
      <i class="icon-globe"></i>
      <i class="icon-search"></i>
        <div id="addressSearch">
            <label for="adressa">Adressuleitan</label>
            <input type="textbox" class="typeahead" name="adressa" placeholder="Skriva adressu her" data-provide="typeahead" data-items="10" />
        </div>
        <div id="timeOnMap"></div>
        <div id="admin">
          <div class="header">
            <a href="/admin/admin.php?manage_trackers=true" id="styr_trackarum">S</a>
            <i id="showOrHideSidebar" class="icon-white icon-chevron-right" title="Fjal ella vís"></i>
            <br style="clear: both" />
            <h1>Eindir</h1>
          </div>
          <div class="units">
          </div>

          <script src="/assets/js/jquery.tools.min.js"></script>
          <script src="/assets/js/jquery.livequery.js"></script>
          <script src="/assets/js/tasks.js"></script>
          <script src="/assets/js/tasks.create.js"></script>
          <script src="/assets/js/tasks.token.js"></script>
          <script>
              var tasks = new Tasks();
              $(document).ready(function () {
                  setTimeout(function () {
                      tasks.init();
                  }, 1500); // I really should user $.Deferred here
                  // I need to load tasks AFTER I have received the task list

              });
          </script>

          <br style="clear: both;" />
          <div class="tasks">
            <div class="headline">
                <h1>Uppgávur</h1>
                <i id="button_add_task" title="Stovna uppgávu" class="icon-white icon-plus"></i>
            </div>
            <div class="ongoing collapsible">
                <h2><i class="icon-chevron-up"></i> Ígongd <span id="number_ongoing"></span></h2>
            </div>
            <div class="notstarted collapsible">
                <h2><i class="icon-chevron-up"></i> Ikki byrjað <span id="number_notstarted"></span></h2>
            </div>
            <div class="done collapsible">
                <h2><i class="icon-chevron-up"></i> Avgreitt <span id="number_done"></span></h2>
            </div>
          </div>

        </div>
      </div>
    </div>
    <div id="copyright">
      <a href="http://munin.fo" target="_blank" title="Vitja munin.fo">&copy; Munin 2012</a>
    </div>
  </body>
  <script id="taskDescription" type="text/x-handlebars">
    <div class="task">
      <div class="description">{{description}}</div>
      <div class="device">
        <div class="symbol"></div>
        <i class="icon-white icon-ok-sign" style="margin-left: 5px; margin-top: 1px;"></i>
        <div class="deviceName">{{name}}</div>
      </div>
      <ul>
      </ul>
    </div>
  </script>
</html>
