<?php
	require_once("../admin/func.php");
    $session = new Session();

    if (user_can_read(get_user_id($_SESSION['email']), $_REQUEST['deviceid'])) {

        if (isset($_POST['x']) && isset($_POST['y'])) {
            $query='INSERT INTO message ("DeviceID", geom, message, sent)
                    VALUES (
                      '.intval($_POST['deviceid']).',
                      '.'st_makepoint(' . $_POST['x'] . ', ' . $_POST['y'] . '),
                      \''.pg_escape_string($_POST['message']).'\',
                      false
                    )
                    RETURNING id'; 

            $result = pg_query($query) OR DIE("sql error");

            die(json_encode((object)array( 'id' => current(pg_fetch_object($result)) )));
        }
        if (isset($_GET['mid'])) {
            $query = "select * from message where sent=true and id = ".intval($_GET['mid']);
            $result = pg_query($query) or DIE("sql error");

            if (pg_num_rows($result) > 0) {
                die(json_encode((object)array('sent' => true)));
            }
            else {
                die(json_encode((object)array('sent' => false)));
            }
        }
    }
    else
        die("not allowed!");
