/*global document, $, tasks, socket, console, map, setInterval, mtData, clearInterval, require, setTimeout */
$(document).on('tasks_ready', function () {
    "use strict";

    /**
     * Function to count number of tasks in the boxes notstarted, ongoing, done. 
     */
    function countTasks() {
        $(['notstarted', 'ongoing', 'done']).each(function (i, type) {
            $('#number_' + type).text($('.' + type + ' .task').length);
        });
    }

    /**
     * Function to order the tasks in the boxes on screen
     *
     * Will first remove the elements in the boxes, and then insert the 
     * tasks in the right box.
     */
    function sortTasks() {
        // Delete tasts
        $('.ongoing .task, .notstarted .task, .done .task', '#admin .tasks').remove();

        // Iterate ver tasks. Put them into the correct box.
        $(tasks.data.tasks).each(function (i, task) {
            if (task.isEnded()) {
                $('#admin .tasks .done').append(task.getDomNode());
            } else if (task.isStarted()) {
                $('#admin .tasks .ongoing').append(task.getDomNode());
            } else {
                $('#admin .tasks .notstarted').append(task.getDomNode());
            }

            // This should be moved into Task
            $(task.htmlElement).hover(function () {
                task.showLocationsOnMap.call(task, map);
            }, function () {
                task.hideLocationsOnMap.call(task, map);
            });
        });
        countTasks();
    }
    $('.tasks').on('tasksupdated', sortTasks);

    var socketInit = function () {
        socket.on('task_status_changed', function (task) {
            console.log('Socket.io event: task_status_changed');
            tasks.update(task.task, function (task) {
                // Update finished
                sortTasks();
                $('.tasks').trigger('tasksupdated');

                var $taskContainer = $(task.htmlElement.parentNode);
                mtData.flasher.addNode($taskContainer);
            });
            $.titleAlert('Boð broytt', {stopOnMouseMove: true});
        });
    };

    // When socket is available, run socketInit
    // This should use $.Deferred
    if (socket !== undefined) {
        socketInit();
    } else {
        $(document).one('socket_ready', socketInit);
    }

    setTimeout(function () {
        tasks.show();
        $('.tasks').trigger('tasksupdated');
    }, 0);

    $('#admin .tasks .collapsible .task').hide();

    $('#admin .tasks h2').click(function (event) {
        $('.task', $(this).parent()).slideToggle(200);
        $('i', this).toggleClass('icon-chevron-up icon-chevron-down');
    });

    /** 
     * Add task: 
     */
    $('#button_add_task').click(function (event) {
        console.log('Add task');
        var createTask = $('<div>'),
            form = document.createElement('div'),
            removeButton = document.createElement('i'),
            button = document.createElement('a'),
            locations = [];

        /**
         * Send task to server
         */
        $(button).append('Send')
            .addClass('btn')
            .click(function () {
                var task = {
                    description: $('input[name=description]', createTask).val(),
                    locations: [],
                    devices: [],
                    token: tasks.token.token
                };

                $('input[name^=place]', createTask).each(function (i, a) {
                    task.locations = locations;
                });

                createTask.find('.deviceElement.active .id').each(function (index, device) {
                    task.devices.push($(device).val());
                });

                tasks.create.save(task, function (deliveredTaskToDevice) {
                    if (deliveredTaskToDevice) {
                        console.log('sucessfully delivered task to device');
                    } else {
                        console.log('could not deliver task to device');
                        // use icon-exclamation-sign 
                        // use icon-ok-sign
                    }

                    console.log("tasks.create.save succes callback: ");

                    require(['/assets/js/Task.js'], function (Task) {
                        tasks.data.tasks.push(new Task({task: task}));
                    });


                    tasks.reload(mtData, function () {
                        $('.tasks').trigger('tasksupdated');
                    });
                });
                $(createTask).remove();
            });
        // END 'Send task to server'


        $(removeButton).addClass('icon-remove').click(function () {
            $(createTask).remove();
        });

        $(createTask).addClass('createTask');
        $(form).addClass('form');
        createTask.css('clear', 'both');

        $(form).append('<label>Frágreiðing: </label><input name="description" />');
        $(form).append('<label>Staðseting: </label>');
        $(form).append('<input name="place[]" class="typeahead" placeholder="Skriva adressu her" id="place" />');

        $('#addressSearch').bind('foundAddress', function (event, address) {
            locations.push(address);
        });

        $(form).append(
            $('<i class="icon-white icon-plus" title="Legg stað afturat"></i>').click(function () {
                $('<input name="place[]" class="typeahead" placeholder="Skriva adressu her" />').insertAfter($('.createTask #place'));
            })
        );

        $(form).append('<label>Eindir: </label>');
        mtData.devices.forEach(function (device) {
            if (device === undefined) {
                console.log("Device is never undefined ;-) ");
                return;
            }

            var deviceElem = $('<div/>').addClass('deviceElement'),
                symbol = $('<div/>')
                    .css('background', device.color.toCss(true))
                    .addClass('symbol');

            deviceElem
                .append(device.devicetitle)
                .append('<input type="hidden" value="' + device.id + '" class="id" />')
                .append(symbol)
                .click(function () {
                    $(this).toggleClass('active');
                });

            $(form).append(deviceElem);
        });
        $(form).append(button);

        $(createTask).prepend('<h2 style="cursor: default;">Stovna uppgávu</h2>');
        $(createTask).append(removeButton, form);

        $(createTask).insertAfter($('#admin .tasks .headline'));
    });
    // END: 'Add task'

    // Count tasks and update task view
    $('.tasks').trigger('tasksupdated');

});

