<?php
require_once("../../admin/.db_connect.php");
//date.php
// interface to lookup in the calendar

$sql = '
SELECT "TimeStamp"::date date, COUNT(1) points
FROM "Tracking" INNER JOIN  (SELECT calendar_date
FROM (
    SELECT
        DATE(date_trunc(\'month\', CURRENT_DATE) +
        (CAST(d AS VARCHAR) || \' days\')::INTERVAL) AS calendar_date
    FROM generate_series(0, 30) AS cal(d)
) AS cal1
WHERE date_trunc(\'month\', calendar_date) =
      date_trunc(\'month\', CURRENT_DATE)
) AS tm ON "TimeStamp"::date = tm.calendar_date

WHERE "DeviceID" = '.$_GET['device'].'
GROUP BY "TimeStamp"::date
ORDER BY "TimeStamp"::date desc;

';
$result = pg_query($sql) or die("You suck at sql!");

$return = array();
while ($row = pg_fetch_object($result)) {
	$return[$row->date] = $row->points;
}


header("Content-type: text/plain");
echo json_encode($return);

