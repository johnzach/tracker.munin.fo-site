/*global $, esri, dojo, iconPaths*/
(function () {
    "use strict";
    function showPauseTimeHover(event) {
        var time = new Date(event.graphic.time),
            now = event.graphic.nextPoint ? new Date(event.graphic.nextPoint.time) : new Date(),
            tid = $('<div />'),
            timar = now - time > 3600000 ? Math.floor((now - time) / 3600000) + 't, ' : false,
            minuttir = now - time > 60 ? Math.floor(((now - time) % 3600000) / 60000) + 'm, ' : false,
            sekund = Math.round(((now - time) % 60000) / 1000) + 's.';

        tid
            .append("Steðgur í " + (timar || '') + (minuttir || '') + (sekund || ''), $('<br />'),
                "Byrjað " + time.toDDMM() + ' ' + time.toHHMM(), $('<br />'),
                "Endað " + now.toDDMM() + ' ' + now.toHHMM())

            .css('top', event.clientY + "px")
            .css('left', event.clientX + "px")

            .addClass('timeonMap')

            .on('mouseleave', function () {
                tid.hide();
            });
        $('#timeOnMap').append(tid);
    }
}());
