<?php   session_start();
        require_once("public_access.php");
        if (empty($_SESSION['email']) && empty($publickey)) // I should implement at least two modes. Embedded and full browser. 
            header("Location: /admin/admin.php");
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="initial-scale=1, maximum-scale=1,user-scalable=no"/>
    <title>GPS-Tracking</title>
    <link rel="stylesheet" href="http://serverapi.arcgisonline.com/jsapi/arcgis/2.5/js/dojo/dijit/themes/tundra/tundra.css" />
    <style>
      html, body { height: 100%; width: 100%; margin: 0; padding: 0; }
      #map {
        margin: 0;
        padding: 0;
        background-color: rgb(153, 179, 204);
      }
      #copyright {
        position: absolute;
        bottom: 12px;
        right: 12px;
        font-size: 10pt;
        font-family: helvetica, arial, sans-serif;
        color: white;
        z-index: 2;
        font-weight: bold;
      }
      #admin {
        font-family: sans-serif;
        color: white;
        opacity: 0.8;
        border-radius: 10px;
        background-color: #777;
        padding: 1.0em;
        position: absolute;
        right: 1em;
        top: 1em;
        z-index: 100000;
        overflow: hidden;
      }
      input[type='checkbox'] {
        float: right;
      }
      #arrow {
        float: left;
        width: 100%;
        height: 10px;
        margin: -2.0em 0em 0 -2.2em;
        padding: 1.5em;
        background-repeat: no-repeat;
        background-position: 90% 50%;
      }
      .timeonMap {
        color: white;
        background-color: black;
        border-radius: 15px;
        padding: 6px;
        font-family: sans-serif;
      }
      #map .container .infowindow .window .right .user .titlebar .hide {
        display: none;
      }
      #map .container .infowindow .window .right .user .border {
        display: none;
      }
      #map .container .infowindow .window .right .user .content {
        overflow: visible;
        margin: 1px 0 2px 1px;
      }

    </style>
    <script>var dojoConfig = { parseOnLoad: true }; </script>
    <script src="http://serverapi.arcgisonline.com/jsapi/arcgis/?v=2.5"></script>
    <script>
        "use strict";
        /* JS Lint */
        //var dojo, dijit, clearTimeout, setTimeout, setInterval, clearInterval, esri, window;
        dojo.require("dijit.layout.BorderContainer");
        dojo.require("dijit.layout.ContentPane");
        dojo.require("esri.map");
        dojo.require("esri.symbol");
        dojo.require("dijit.form.Slider");


        var map, resizeTimer, debug;
        debug = false;
        var deviceParams;

        function hideTime() {
            //console.log("Clear time bubbles: " + new Date().getMilliseconds());
            if (dojo.isIE <= 8) {
                return;
            }
            var times = dojo.doc.getElementsByClassName("timeonMap");
            dojo.forEach(times, function (time) {
                time.parentNode.removeChild(time);
            });
        }

        // Draw a route for every registered device
        function drawRoute(polylineJson, device, centerat) {
            var rgb, polylineSymbol, runningRoute, color, time, timeC, pict, mynd, it;
            //console.log("drawRoute: " + new Date().getMilliseconds());

            // Define the color
            rgb = device.RGB.substr(1);
            rgb = rgb.substr(0, rgb.length - 1);
            rgb = rgb.split(",");
            color = new dojo.Color([rgb[0], rgb[1], rgb[2], 0.6]);


            // Update the time
            time = dojo.byId("time" + device.id);
            time.removeChild(time.firstChild);
            timeC = dojo.doc.createTextNode(device.datetime);
            time.appendChild(timeC);

            polylineSymbol = new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, color, 3);
            runningRoute = new esri.geometry.Polyline(polylineJson);
            map.graphics.add(new esri.Graphic(runningRoute, polylineSymbol));

            if (!(dojo.isIE <= 8)) {
            dojo.forEach(polylineJson.paths[0], function (point) {
                var geoPoint, marker, graphic;
                geoPoint = new esri.geometry.Point(point[0], point[1], map.spatialReference);
                marker = new esri.symbol.SimpleMarkerSymbol(esri.symbol.SimpleMarkerSymbol.STYLE_SQUARE, 20, new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol, new dojo.Color([255, 0, 0, 0]), 1), new dojo.Color([0, 255, 0, 0]));

                graphic = new esri.Graphic(geoPoint, marker);
                graphic.time = point[2];
                map.graphics.add(graphic);
            });

                dojo.connect(map.graphics, 'onMouseOver', function (event) {
                    hideTime();
                    if (event.graphic.time) {
                        var time, tid, tidE, timeOnMap;
                        time = new Date(event.graphic.time);
                        var timestring = time.getDate() + "/" + (parseInt(time.getMonth(), 10)+1) + ", " + time.toLocaleTimeString();
                        //time = new Date(time.setHours(time.getHours() - 1));
                        tid = dojo.doc.createElement("div");
                        tidE = dojo.doc.createTextNode(timestring);
                        tid.appendChild(tidE);
                        tid.style.position = "absolute";
                        tid.style.top = event.clientY + "px";
                        tid.style.left = event.clientX + "px";
                        tid.style.zIndex = 1000000;
                        tid.className = "timeonMap";
                        timeOnMap = dojo.byId("timeOnMap");
                        timeOnMap.appendChild(tid);
                    }
                });
            dojo.connect(map.graphics, 'onMouseOut', function () { hideTime(); });
            }

            if (device.url && device.width && device.height) {
                map.infoWindow.setContent("<img src='" + device.url + "' width='50px' height='50px' />");
                map.infoWindow.show(runningRoute.getPoint(0, 0));
                map.infoWindow.resize(60, 60);
            } else {
                map.graphics.add(new esri.Graphic(runningRoute.getPoint(0, 0), new esri.symbol.SimpleMarkerSymbol(esri.symbol.SimpleMarkerSymbol.STYLE_DIAMOND, 20, new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([0, 0, 0]), 1), color)));
            }
            if (centerat === true) {
                map.centerAt(runningRoute.getPoint(0, 0));
            }
        }

        function get_position(devices) {
            //console.log("Get position: " + new Date().getMilliseconds());
            hideTime();
            //console.log("HideTime done.");
            if (!(dojo.isIE < 9)) {
                map.graphics.clear();
                console.log("Graphics on map cleared. ");
            }
            dojo.forEach(devices, function (device) {
                //console.log("Get line for device " + device.id + ": " + new Date().getMilliseconds());
                if (dojo.byId("visible" + device.id).checked) {
                    dojo.xhrGet({
                        url: "get_last_line2.php",
                        // Allow only 1 second to get position
                        timeout: 3000,
                        content: {
                            deviceid: device.id,
                            points: dojo.byId("sliderValue" + device.id).value
                        },
                        // The success callback with result from server
                        load: function (json) {
                            var result, centerat;
                            result = dojo.fromJson(json);
                            centerat = false;
                            if (devices.length === 1) {
                                centerat = true;
                            }
                            if (result.paths instanceof Array) {
                                drawRoute(result, device, centerat);
                            }
                        } // End load: json
                    }); // End dojo.xhrGet
                }
            });
        } // End get_position

        function slide_box(run) {
            var arrow, adminKassi, adminKassiHeight, adminKassiWidth;
            arrow = dojo.byId("arrow");
            adminKassi = dojo.byId("admin");
            if (dojo.isIE <= 8) {
                adminKassi.style.display = "none";
            }
            adminKassiHeight = window.getComputedStyle(adminKassi).height;
            adminKassiWidth = window.getComputedStyle(adminKassi).width;
            adminKassi.style.height = adminKassiHeight;

            var styrTrackarum = dojo.byId("styr_trackarum");

            function showorhide() {
                if (parseInt(adminKassi.style.height, 10) === 10) {
                    dojo.animateProperty({
                        node: "admin",
                        properties: {
                            height: {
                                end: parseInt(adminKassiHeight, 10)
                            },
                            width: {
                                end: parseInt(adminKassiWidth, 10)
                            }
                        }
                    }).play();
                    styr_trackarum.style.display = "";
                    arrow.style.backgroundImage = "url(/img/arrow/down.png)";
                } else {
                    dojo.animateProperty({
                        node: "admin",
                        properties: {
                            height: {
                                end: 10
                            },
                            width: {
                                end: 10
                            }
                        }
                    }).play();
                    styr_trackarum.style.display = "none";
                    arrow.style.backgroundImage = "url(/img/arrow/up.png)";
                }
            }
            if (!run) {
                arrow.style.backgroundImage = "url(/img/arrow/down.png)";
                dojo.connect(arrow, 'onclick', showorhide);
            } else {
                showorhide();
            }
        }

        function centerOnTracker(device) {
            var data, line, lastpos;
            dojo.xhrGet({
                url: "get_last_line.php",
                timeout: 6000,
                content: {
                    deviceid: device.id,
                    points: 2
                },
                load: function (json) {
                    data = dojo.fromJson(json);
                    line = new esri.geometry.Polyline(data);
                    lastpos = line.getPoint(0, 0);
                    map.centerAt(lastpos);
                }
            });
        }

        function displayDevices(devices) {
            //console.log("displayDevices: " + new Date().getMilliseconds());
            if (debug === true) {
                return;
            }
            debug = true;
            var container = dojo.byId("admin");

            dojo.forEach(devices, function (device) {
                var rgb, color, now, deviceInfo, br, name, nameContent, checkbox, timespan, time, slider, sliderValue;

                rgb = device.RGB.substr(1);
                rgb = rgb.substr(0, rgb.length - 1);
                rgb = rgb.split(",");

                color = new dojo.Color([rgb[0], rgb[1], rgb[2], 0.6]);
                now = new Date();

                deviceInfo = dojo.doc.createElement("div");

                br = dojo.doc.createElement("br");
                deviceInfo.appendChild(br);

                name = dojo.doc.createElement("div");
                name.style.color = color.toHex();
                nameContent = dojo.doc.createTextNode(device.devicetitle + ", ");

                timespan = dojo.doc.createElement("span");
                timespan.setAttribute("id", "time" + device.id);
                time = dojo.doc.createTextNode(device.datetime);
                timespan.appendChild(time);

                name.appendChild(nameContent);
                name.appendChild(timespan);

                deviceInfo.appendChild(name);

                checkbox = dojo.doc.createElement("input");
                checkbox.setAttribute("type", "checkbox");

                //if (device.datetime.substr(0,2) == now.getDate())
                checkbox.checked = true;

                checkbox.setAttribute("id", "visible" + device.id);
                deviceInfo.appendChild(checkbox);

                dojo.connect(name, "onclick", function () {
                    checkbox.checked = true;
                    centerOnTracker(device);
                    get_devices();
                });

                dojo.connect(checkbox, "onclick", function (evt) {
                    if (evt.ctrlKey || evt.metaKey) { // CTRL on windows, CMD on mac
                        dojo.query('input[type=checkbox]', container).forEach(function (box) {
                            box.checked = checkbox.checked;
                        });
                    }
                    get_devices();
                });


                slider = dojo.doc.createElement("div");
                slider.setAttribute("id", "slider" + device.id);
                deviceInfo.appendChild(slider);

                sliderValue = dojo.doc.createElement("input");
                sliderValue.setAttribute("type", "textbox");
                sliderValue.style.display = "none";
                sliderValue.setAttribute("value", "2");
                sliderValue.setAttribute("id", "sliderValue" + device.id);
                deviceInfo.appendChild(sliderValue);

                dojo.place(deviceInfo, container, "last");

                dojo.addOnLoad(function () {
                    dijit.form.HorizontalSlider({
                        name: "slider" + device.id,
                        value: 2,
                        minimum: 2,
                        maximum: 1000,
                        intermediataChanges: true,
                        style: "width: 200px;",
                        onChange: function (value) {
                            dojo.byId("sliderValue" + device.id).value = value;
                            get_devices();
                        }
                    }, "slider" + device.id);
                });

            });
            slide_box();
<?php if ($_GET['user'] == "gmf@kringvarp.fo") echo "
            slide_box(true);\n"; ?>
        } // End function displayDevices


        function get_devices() {
            //console.log("Get devices: " + new Date().getMilliseconds());
            dojo.xhrGet({
                url: "get_devices.php",
                timeout: 1000,
                load: function (json) {
                    //console.log("Devices data loaded - callback. " + new Date().getMilliseconds());
                    var devices = dojo.fromJson(json);
                        //console.log("Call to displayDevices: " + new Date().getMilliseconds());
                    displayDevices(devices);
                        //console.log("Call to get_position: " + new Date().getMilliseconds());
                    get_position(devices);
                }
            });
        }


        function init() {
            var initExtent, basemapURL, basemap, Poll, p;
            initExtent = new esri.geometry.Extent({"xmin": 584390, "ymin": 6868745, "xmax": 638036, "ymax": 6891755, "spatialReference": {"wkid": 32629}});
            map = new esri.Map("map", { extent: initExtent, logo: false});
//            if (dojo.isIE <= 8) {
//                alert("GG. Hendan skipanin fungerar ikki optimalt til eldri útgávur av Internet Explorer. Munin viðmælur Google Chrome.");
//            }

//          basemapURL = "http://www.kort.fo:8089/ArcGIS/rest/services/framloga/tk_grundkort/MapServer"; // Munin grundkort
//            basemapURL = "http://www.kort.fo:8089/ArcGIS/rest/services/munin/Muninkort-vegir2/MapServer"; // Munin vegir
            basemapURL = "http://ags10.kort.fo/ArcGIS/rest/services/munin/Muninkort-vegir2/MapServer"; // Munin vegir
//            basemapURL = "http://ags10.munindev.tk/ArcGIS/rest/services/munin/Muninkort-vegir2/MapServer"; // Munin vegir
            
          basemap = new esri.layers.ArcGISTiledMapServiceLayer(basemapURL);

//            basemapURL = "https://www.kort.fo/ArcGIS/rest/services/Munin/MuninKortF20_2/MapServer"; // Munin grundkort
//            basemapURL = "http://www.kort.fo:8089/ArcGIS/rest/services/munin/Muninkort-vegir/MapServer"; // Nýtt kort 
//            basemapURL = "http://munindev.tk/ArcGIS/rest/services/munin/Muninkort-vegir/MapServer"; // Cacheserver
//            basemap = new esri.layers.ArcGISDynamicMapServiceLayer(basemapURL);
            map.addLayer(basemap);

            dojo.connect(map, 'onLoad', function () {
                dojo.connect(dijit.byId('map'), 'resize', function () {
                    clearTimeout(resizeTimer);
                    resizeTimer = setTimeout(function () {
                        map.resize();
                        map.reposition();
                    }, 500);
                });
            });

            Poll = function (pollFunction, intervalTime) {
                var intervalId = null;

                this.start = function (newPollFunction, newIntervalTime) {
                    pollFunction = newPollFunction || pollFunction;
                    intervalTime = newIntervalTime || intervalTime;

                    if (intervalId) {
                        this.stop();
                    }
                    intervalId = setInterval(pollFunction, intervalTime);
                };

                this.stop = function () {
                    clearInterval(intervalId);
                };
            };

            // Poll for new devices and positions every 3 seconds
            p = new Poll(get_devices, 15000);
            p.start();
            get_devices();
        } // End function init

        dojo.ready(init);
    </script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-26546469-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
  </head>

  <body class="tundra">
    <div data-dojo-type="dijit.layout.BorderContainer"
         data-dojo-props="design:'headline',gutters:false"
         style="width: 100%; height: 100%; margin: 0;">
      <div id="map"
           data-dojo-type="dijit.layout.ContentPane"
           data-dojo-props="region:'center'">
      <div id="timeOnMap"></div>

        <div id="admin">
          <?php if ($_SESSION['email']): ?>
          <a href="/admin/admin.php?manage_trackers=true" style="color:white; text-decoration: none;" id="styr_trackarum">Stýr trackarum</a>
          <?php endif; ?>
          <span id="arrow">
          </span>
        </div>
      </div>
    </div>
    <div id="copyright"><a href="http://munin.fo" target="_blank" style="color: black; text-decoration: none;">&copy; Munin 2011</a></div>
  </body>
</html>
