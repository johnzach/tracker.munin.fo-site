<?php
	ob_start("ob_gzhandler");

	// I should really use some sort of authentication here
	require_once("../admin/func.php");
	session_start();
	header("Content-type: application/json");
	if (empty($_SESSION['email']))
		die("Innrita fyrst");

	ini_set('display_errors',1);
	error_reporting(E_ALL);


	class Geojson 
	{
		protected $data = array();

		public function addLineString($lineString)
		{
			// Assuming the structure is like this:
			// LINESTRING(123.123 123.123)
			$lineString = substr($lineString, 11, strlen($lineString)-12);

			$points = explode(",", $lineString);

			foreach ($points as &$point)
			{
				$pointData = array_combine(array('x','y'),explode(' ',$point));
				$point = array((float)$pointData['x'], (float)$pointData['y']);
			}

//			$this->data['geometry']['type'][] = "LineString";
			$this->data['paths'][] = $points;
		}

		public function setSpatialReference($wkid)
		{
			$this->data['spatialReference']['wkid'] = $wkid;
		}

		public function dumpData()
		{
			return "<pre>".print_r($this->data,true)."</pre>";
		}
		public function toJson()
		{
			return json_encode($this->data);
		}
	}

	$device_id = pg_escape_string($_GET['deviceid']);
	$points = pg_escape_string($_GET['points']);
	$userID = get_user_id($_SESSION['email']);
	
	if (!user_can_read($userID, $device_id))
		die("Hesin brúkari hevur ikki rættindi til hesa eindina");


	$dbconn = pg_connect("host=localhost port=5432 dbname=tracking user=postgres");

	if (!$dbconn)
		echo "Ein feilur er hendur, kann ikki knýta til dátugrunn.";

	$result = pg_query('SELECT st_asewkt(st_makeline(the_geom)), "DeviceID"
			    FROM (SELECT the_geom, "DeviceID"
				  FROM "Tracking" 
				  WHERE "DeviceID" = \''.$device_id.'\'
				  ORDER BY "ID" DESC LIMIT '.intval($points).') as sq
				  GROUP BY "DeviceID";');

	if (!$result)
		die("DB-ERROR!");
	
	$geojson = new geojson();

	if ($row = pg_fetch_row($result)) {
		$row = current($row);
		$geojson->addLineString($row);
	}

	$geojson->setSpatialReference(32629);

	echo $geojson->toJson();
	//echo $geojson->dumpData();

