<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="initial-scale=1, maximum-scale=1,user-scalable=no"/>
    <title>GPS-Tracking</title>
    <link rel="stylesheet" href="http://serverapi.arcgisonline.com/jsapi/arcgis/2.4/js/dojo/dijit/themes/tundra/tundra.css" />
    <style>
      html, body { height: 100%; width: 100%; margin: 0; padding: 0; }
      #map{ margin: 0; padding: 0; }
    </style>
    <script>var dojoConfig = { parseOnLoad: true }; </script>
    <script src="http://serverapi.arcgisonline.com/jsapi/arcgis/?v=2.4"></script>
    <script>
      "use strict";
      dojo.require("dijit.layout.BorderContainer");
      dojo.require("dijit.layout.ContentPane");
      dojo.require("esri.map");
      
      var map, resizeTimer;
      function init() {
        var initExtent, basemapURL, basemap, tk_adressurURL, tk_adressur, Poll, p; 
        initExtent = new esri.geometry.Extent({"xmin": 584390, "ymin": 6868745, "xmax": 638036, "ymax": 6891755, "spatialReference": {"wkid": 32629}});
        map = new esri.Map("map", { extent: initExtent, logo: false});

        basemapURL = "http://www.kort.fo:8089/ArcGIS/rest/services/framloga/tk_grundkort/MapServer"; // Munin grundkort
        basemap = new esri.layers.ArcGISTiledMapServiceLayer(basemapURL);
        map.addLayer(basemap);

        tk_adressurURL = "http://www.kort.fo:8089/ArcGIS/rest/services/framloga/tk_adressur/MapServer"; // adressur
        tk_adressur = new esri.layers.ArcGISTiledMapServiceLayer(tk_adressurURL);
        map.addLayer(tk_adressur);
	

        dojo.connect(map, "onLoad", doQueries);
        dojo.connect(map, 'onLoad', function () { 
          dojo.connect(dijit.byId('map'), 'resize', function () {  
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(function () {
              map.resize();
              map.reposition();
            }, 500);
          });
        });

        Poll = function (pollFunction, intervalTime) {
          var intervalId = null;

          this.start = function (newPollFunction, newIntervalTime) {
            pollFunction = newPollFunction || pollFunction;
            intervalTime = newIntervalTime || intervalTime;

            if (intervalId) {
              this.stop();
            }

            intervalId = setInterval(pollFunction, intervalTime);
          };

          this.stop = function () {
            clearInterval(intervalId);
          };
        };

        p = new Poll(get_position, 3000);
        p.start();
      }


      function get_position() {
        // Using dojo.xhrGet, as very little information is being sent
        dojo.xhrGet({
          url: "get_last_position.php",
          // Allow only 1 second to get position
          timeout: 1000,
          content: {
            projection: 'utm29n'
          },
          // The success callback with result from server
          load: function (json) {
            var result, pos;
            result = dojo.fromJson(json);
            pos = new esri.geometry.Point({"x": result.x, "y": result.y, "spatialReference": {"wkid": 32629} });
            map.graphics.clear();
            map.graphics.add(new esri.Graphic(pos, new esri.symbol.SimpleMarkerSymbol(esri.symbol.SimpleMarkerSymbol.STYLE_CIRCLE, 10, new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([255, 0, 0, 0.8]), 1), new dojo.Color([255, 255, 255, 0.25]))));
          } // End load: json
        }); // End dojo.xhrGet
      } // End get_position


      function doQueries(map) {
        dojo.xhrGet({
          url: "get_last_line.php",
          timeout: 3000,
          content: {
            projection: 'utm29n'
          },
          load: function(json) {
            var featureSet = dojo.fromJson(json);
            var symbol = new esri.symbol.SimpleMarkerSymbol();
            symbol.setColor(new dojo.Color([0,0,255]));

            //Create graphics layer for cities
            var lineLayer = new esri.layers.GraphicsLayer();
            map.addLayer(lineLayer);
            map.reorderLayer(lineLayer,1);

            var infoTemplate = new esri.InfoTemplate("${CITY_NAME}","${*}");

            //Add cities to the graphics layer
            dojo.forEach(featureSet.features, function(feature) {
              lineLayer.add(feature.setSymbol(symbol).setInfoTemplate(infoTemplate));
            });
          }
        });
      }

      dojo.ready(init);
    </script>
  </head>
  
  <body class="tundra">
    <div data-dojo-type="dijit.layout.BorderContainer" 
         data-dojo-props="design:'headline',gutters:false" 
         style="width: 100%; height: 100%; margin: 0;">
      <div id="map" 
           data-dojo-type="dijit.layout.ContentPane" 
           data-dojo-props="region:'center'"> 
      </div>
    </div>
  </body>
</html>


