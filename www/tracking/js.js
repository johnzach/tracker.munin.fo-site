/*global mtConfig, window, $, document, console, clearTimeout, setTimeout, clearInterval, setInterval, tasks, require, Task, dojo, esri */
// Waaay too many globals
var socket, mtData, map,
    iconPaths = {
        car: 'M28.59,10.781h-2.242c-0.129,0-0.244,0.053-0.333,0.133c-0.716-1.143-1.457-2.058-2.032-2.633c-2-2-14-2-16,0C7.41,8.854,6.674,9.763,5.961,10.898c-0.086-0.069-0.19-0.117-0.309-0.117H3.41c-0.275,0-0.5,0.225-0.5,0.5v1.008c0,0.275,0.221,0.542,0.491,0.594l1.359,0.259c-1.174,2.619-1.866,5.877-0.778,9.14v1.938c0,0.553,0.14,1,0.313,1h2.562c0.173,0,0.313-0.447,0.313-1v-1.584c2.298,0.219,5.551,0.459,8.812,0.459c3.232,0,6.521-0.235,8.814-0.453v1.578c0,0.553,0.141,1,0.312,1h2.562c0.172,0,0.312-0.447,0.312-1l-0.002-1.938c1.087-3.261,0.397-6.516-0.775-9.134l1.392-0.265c0.271-0.052,0.491-0.318,0.491-0.594v-1.008C29.09,11.006,28.865,10.781,28.59,10.781zM7.107,18.906c-1.001,0-1.812-0.812-1.812-1.812s0.812-1.812,1.812-1.812s1.812,0.812,1.812,1.812S8.108,18.906,7.107,18.906zM5.583,13.716c0.96-2.197,2.296-3.917,3.106-4.728c0.585-0.585,3.34-1.207,7.293-1.207c3.953,0,6.708,0.622,7.293,1.207c0.811,0.811,2.146,2.53,3.106,4.728c-2.133,0.236-6.286-0.31-10.399-0.31S7.716,13.952,5.583,13.716zM24.857,18.906c-1.001,0-1.812-0.812-1.812-1.812s0.812-1.812,1.812-1.812s1.812,0.812,1.812,1.812S25.858,18.906,24.857,18.906z',
        pause: 'M428.719,78.321c-114.438,0.264-207.672,93.563-207.764,207.914 c-0.092,114.434,93.607,208.124,208.098,208.082c114.625-0.042,208.041-93.585,207.881-208.166 C636.776,171.428,543.233,78.059,428.719,78.321z M393.428,360.919c-0.01,7.73-1.195,15.225-5.494,21.926 c-5.188,8.086-12.209,13.269-22.127,12.874c-10.344-0.412-17.137-6.534-21.746-15.271c-3.248-6.154-4.156-12.905-4.15-19.812v-0.001 l0,0c0.023-21.313,0.01-42.626,0.01-63.939c0-21.479,0.002-42.959-0.002-64.438l0,0c0,0,0,0,0-0.001 c-0.002-9.23,2.055-17.842,7.938-25.175c11.781-14.69,32.348-11.8,41.348,5.674c1.744,3.387,2.82,6.901,3.455,10.5 c0.537,3.042,0.764,6.144,0.768,9.287C393.473,275.334,393.473,318.127,393.428,360.919z M517.983,362.358 c0.004,7.485-1.541,14.523-5.635,20.855c-5.111,7.901-12.129,12.89-21.828,12.497c-10.318-0.417-17.115-6.442-21.742-15.228 c-3.174-6.029-4.109-12.534-4.086-19.294c0.076-21.478,0.027-42.956,0.027-64.434c0.002-21.646,0.051-43.29-0.027-64.934 c-0.025-7.652,1.406-14.868,5.516-21.4c5.027-7.989,12.105-13.075,21.715-12.78c10.234,0.313,17.215,6.29,21.887,15.08 c3.025,5.691,4.18,11.838,4.174,18.271C517.954,274.78,517.954,318.569,517.983,362.358z'
    };




require([
    'dojo/_base/array',
    'dojo/_base/Color',
    'dojo/on',
    'dojo/dom',
    'dojo/dom-attr',
    'esri/layers/GraphicsLayer'
], function (array, Color, on, dom, domAttr, GraphicsLayer) {
    "use strict";

    var resizeTimer, pxWidth;

    /**
     * mtData (MuninTracker Data) is a globally declared variable,
     * holding all the application data and state.
     *
     */
    mtData = {
        devices: [],
        getDevice: function (id) {
            /**
             * Defining device outside forEach function scope
             * to be able to return it
             */
            var device;
            this.devices.forEach(function (deviceIterator) {
                if (deviceIterator.id === id) {
                    device = deviceIterator;
                }
            });
            return device;
        },
        flasher: {
            nodes: [],
            addNode: function (node) {
                var i = mtData.flasher.nodes.push($(node)) - 1;
                $(node).click(function () {
                    // Note: This can easily delete wrong flashing task.
                    node.removeClass('flash');
                    mtData.flasher.nodes.splice(i, 1);
                });
            },
            // Poll runs every second!
            poll: function () {
                mtData.flasher.nodes.forEach(function (node) {
                    node.toggleClass('flash');
                });
            },
            init: function () {
                setInterval(mtData.flasher.poll, 500);
            }
        }
    };


    function breakLineToParts(line, color) {
        var i, timeOnCurrentPoint, timeOnNextPoint,
            points = line.paths[0],
            symbol = new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, color, 3),
            currentLine = [],
            lineGraphics = [];

        // Iterate over all shown points
        // except the last one (referencing i + 1)
        for (i = 0; i < points.length - 1; i += 1) {
            // Add current point to current line.
            currentLine.push(points[i]);

            // Make it possible to see date differences
            timeOnCurrentPoint = new Date(points[i + 1][2]);
            timeOnNextPoint = new Date(points[i][2]);

            // If line is terminated at next point, create new line
            // OR if timedifference is larger than 15 minutes, create new line
            if (points.length === (i + 2) || Math.abs(timeOnCurrentPoint - timeOnNextPoint) > (15 * 60 * 1000)) {
                lineGraphics.push(new esri.Graphic(new esri.geometry.Polyline({
                    paths: [currentLine]
                }), symbol));
                // reset current line when new line is created
                currentLine = [];
            }
        }
        return lineGraphics;
    }

    function hideTime() {
        var times = document.getElementsByClassName("timeonMap");
        array.forEach(times, function (time) {
            time.parentNode.removeChild(time);
        });
    }

    function rgbToColor(RGB, alpha) {
        if (alpha === undefined) {
            alpha = 1.0;
        }
        var rgb = RGB.substr(1);
        rgb = rgb.substr(0, rgb.length - 1);
        rgb = rgb.split(",");
        return new Color([rgb[0], rgb[1], rgb[2], 0.6]);
    }

    function formatDate(time) {
        var day = (time.getDate() < 10) ? "0" + time.getDate() : time.getDate(),
            month = ((time.getMonth() + 1) < 10) ? "0" + (time.getMonth() + 1) : time.getMonth() + 1,
            hours = (time.getHours() < 10) ? "0" + time.getHours() : time.getHours(),
            minutes = time.getMinutes() < 10 ? "0" + time.getMinutes() : time.getMinutes();
        return day + "/" + month + ", " + hours + ":" + minutes;
    }

    function showPauseTimeHover(event) {
        var time = new Date(event.graphic.time),
            now = event.graphic.nextPoint ? new Date(event.graphic.nextPoint.time) : new Date(),
            tid = $('<div />'),
            timar = now - time > 3600000 ? Math.floor((now - time) / 3600000) + 't, ' : false,
            minuttir = now - time > 60 ? Math.floor(((now - time) % 3600000) / 60000) + 'm, ' : false,
            sekund = Math.round(((now - time) % 60000) / 1000) + 's.';

        tid
            .append("Steðgur í " + (timar || '') + (minuttir || '') + (sekund || ''), $('<br />'),
                "Byrjað " + time.toDDMM() + ' ' + time.toHHMM(), $('<br />'),
                "Endað " + now.toDDMM() + ' ' + now.toHHMM())

            .css('top', event.clientY + "px")
            .css('left', event.clientX + "px")

            .addClass('timeonMap')

            .on('mouseleave', function () {
                tid.hide();
            });
        $('#timeOnMap').append(tid);
    }

    function addTrackerPointToMap(runningRoute, device, timearr, lineGraphics) {
        var point = runningRoute.getPoint(0, 0),
            outline = new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([0, 0, 0]), 1),
            symbol = new esri.symbol.SimpleMarkerSymbol(esri.symbol.SimpleMarkerSymbol.STYLE_DIAMOND, 20, outline, device.getIconColor()).setPath(iconPaths.car),
            pointGraphic = new esri.Graphic(point, symbol),
            textSymbol = new esri.symbol.TextSymbol(device.devicetitle).setOffset(10, 15);

        // If timearr is array, contains elements and the first element has a not null value speed, add speed to the title
        if ((timearr instanceof Array) && timearr.length > 0 && timearr[0].hasOwnProperty('speed') && (timearr[0].speed !== null && timearr[0].stop === null)) {
            textSymbol = new esri.symbol.TextSymbol(device.devicetitle + ", " + Math.round(timearr[0].speed * 36) / 10 + "km/t").setOffset(10, 15);
        }


        // Clear the graphics layerfor device
        device.graphics.clear();


        // Append the lines to graphics layer
        lineGraphics.forEach(function (lineGraphic) {
            device.graphics.add(lineGraphic);
        });

        // Append the car icon (latest point)
        if (!timearr || timearr.length < 1 || timearr[0].stop === null) {
            device.graphics.add(pointGraphic);
        }

        // Add text symbol
        device.graphics.add(new esri.Graphic(point, textSymbol));

        // Add all elements from timeArr.
        while (timearr && timearr.length > 0) {
            device.graphics.add(timearr.pop());
        }
    }

    // Draw a route for every registered device
    function drawRoute(polylineJson, device) {
        // Ouch, a lot of unused variable - cleanup needed.
        var pointGraphic,
            pointNameGraphic,
            lineGraphics,
            timearr = [],
            runningRoute,
            colorAlpha = rgbToColor(device.RGB, 0.2),
            time,
            marker,
            pauseMarker,
            timer,
            point,
            outline,
            symbol,
            graphic;



        // Update the time
        time = dom.byId("time" + device.id);
        if (time.firstChild) {
            time.removeChild(time.firstChild);
        }
        time.appendChild(document.createTextNode(device.datetime));

        runningRoute = new esri.geometry.Polyline(polylineJson);

        lineGraphics = breakLineToParts(polylineJson, device.color);

        // If time should be displayed on point: (it always is displayed)
        marker = new esri.symbol.SimpleMarkerSymbol(
            esri.symbol.SimpleMarkerSymbol.STYLE_SQUARE,
            20,
            new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_NULL),
            new Color([0, 255, 0, 0])
        );

        pauseMarker = new esri.symbol.SimpleMarkerSymbol();
        pauseMarker.setPath(iconPaths.pause);
        pauseMarker.setColor(device.getPauseIconColor());
        pauseMarker.setSize("20");
        pauseMarker.setOutline(
            new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID,
                new Color([0, 0, 0]),
                15)
        );

        array.forEach(polylineJson.paths[0], function (point, index) {
            /**
             * Please refactor to using Point collection
             **/
            var geoPoint = new esri.geometry.Point(point[0], point[1], map.spatialReference);

            if (point[6] === null) {
                graphic = new esri.Graphic(geoPoint, marker);
            } else {
                graphic = new esri.Graphic(geoPoint, pauseMarker);
            }
            graphic.time = point[2];
            graphic.speed = point[4];
            graphic.accuracy = point[5];
            graphic.stop = point[6];
            if (polylineJson.paths[0][index - 1] === undefined) {
                graphic.nextPoint = false;
            } else {
                graphic.nextPoint = {
                    time: polylineJson.paths[0][index - 1][2]
                };
            }
            timearr.push(graphic);
        });


        device.setNewGraphicsEventHandler(on(device.graphics, 'MouseOver', function (event) {
            var now, tid;
            window.clearInterval(timer);
            hideTime();
            if (event.hasOwnProperty('graphic') && event.graphic.hasOwnProperty('time') && event.graphic.stop === null) {

                outline = new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([0, 0, 0, 0.2]), 0.5);
                if (event.graphic.accuracy === null) {
                    event.graphic.setSymbol(new esri.symbol.SimpleMarkerSymbol(esri.symbol.SimpleMarkerSymbol.STYLE_CIRCLE, 0, outline, colorAlpha));
                } else {
                    event.graphic.setSymbol(new esri.symbol.SimpleMarkerSymbol(esri.symbol.SimpleMarkerSymbol.STYLE_CIRCLE, event.graphic.accuracy / pxWidth, outline, colorAlpha));
                }
                dojo.connect(event.srcElement, "onmouseout", function () {
                    window.setTimeout(function () {
                        event.graphic.hide();
                    }, 500);
                });
                dojo.connect(event.srcElement, "onclick", function () {
                    hideTime();
                    var ferd;
                    time = new Date(event.graphic.time);
                    tid = document.createElement("div");
                    tid.appendChild(document.createTextNode(time.getDate() + "/" + (parseInt(time.getMonth(), 10) + 1) + ", " + time.toLocaleTimeString()));

                    tid.style.top = event.clientY + "px";
                    tid.style.left = event.clientX + "px";

                    if (event.graphic.speed !== null) {
                        ferd = document.createElement("div");
                        ferd.appendChild(document.createTextNode("Ferð: " + Math.round(event.graphic.speed * 36) / 10 + "km/t"));
                        dojo.addClass(ferd, "ferd");
                        tid.appendChild(ferd);
                    }

                    tid.className = "timeonMap";
                    dojo.byId("timeOnMap").appendChild(tid);
                });
            } else if (event.graphic.stop !== null && event.graphic.hasOwnProperty('time')) {
                // Show pause time...
                showPauseTimeHover(event);
            }
        }));

        on(device.graphics, 'mouseleave', function (event) {
            timer = window.setTimeout(function () {
                hideTime();
                if (event.hasOwnProperty('graphic') && event.graphic.hasOwnProperty('time') && event.graphic.time) {
                    event.graphic.hide();
                }
            }, 200);
            event.stopPropagation();
        });

        addTrackerPointToMap(runningRoute, device, timearr, lineGraphics);


        if (mtData.devices.length === 1) {
            map.centerAt(runningRoute.getPoint(0, 0));
        }

        device.lastpos = runningRoute.getPoint(0, 0);
        device.datetime = formatDate(new Date(polylineJson.paths[0][0][2]));
    }

    function centerOnTracker(device) {
        var point = device.lastpos;
        map.centerAt(new esri.geometry.Point(point, map.spatialReference));
    }

    function displayDevice(device) {
        var container = $("#admin"),
            deviceInfo = $('<div />').attr('deviceId', device.id).addClass('unit'),
            name = $('<div />').css('cursor', 'pointer').addClass('name'),
            nameContent = device.devicetitle + ", ",
            checkbox = $('<input type="checkbox" />').attr('id', 'visible' + device.id).attr('checked', 'true'),
            timespan = $('<span />').attr('id', 'time' + device.id),
            time = device.datetime || '',
            slider = $('<div />').attr('id', 'slider' + device.id),
            symbol = $('<div />').addClass('symbol').css('background', device.color.toCss(true));


        if (!device.hasOwnProperty('datetime')) {
            device.datetime = '';
        }

        timespan.append(time);

        name.append(nameContent, timespan);

        deviceInfo.append(symbol, name, checkbox);

        if (mtConfig.slider.enabled !== false) {
            deviceInfo.append(slider);
        }

        name.click(function () {
            checkbox.checked = true;
            centerOnTracker(device);
        });

        /**
         * Funktionalitetur til at tendra og sløkkja allar 
         * checkboxirnar í einum. 
         *
         * Checkboksin indikerar um eindin er víst á korti
         */
        checkbox.click(function (evt) {
            var checked = this.checked;
            if (evt.ctrlKey || evt.metaKey) { // CTRL on windows, CMD on mac
                $('input[type=checkbox]', container).each(function (i, box) {
                    box.checked = checked;
                });
            }
        });

        $('#admin .units').append(deviceInfo);
        device.addSlider("slider" + device.id);
    }

    // Struct to hold Point data
    function Point(pt) {
        this.x = pt[0];
        this.y = pt[1];
        this.timestamp = new Date(pt[2]);
        this.id = pt[3];
        this.speed = pt[4];
        this.accuracy = pt[5];
        this.stop = (pt[6] === 'stop');
    }

    /**
     * PointCollection 
     * Every device has a PointCollection attached.
     */
    function PointCollection() {
        var uniqueIds = {},
            collection = [];

        this.getPoints = function () {
            return collection;
        };

        this.getLastPoint = function () {
            if (collection.length > 1) {
                return collection[collection.length - 1];
            }
        };

        this.addPoints = function (pointSet) {
            var collectionLength = collection.length,
                points = pointSet.paths[0]
                    .map(function (pt) {
                        return new Point(pt);
                    });

            points.forEach(function (point) {
                if (uniqueIds[point.id] === undefined) {
                    uniqueIds[point.id] = true;
                    collection.push(point);

                    // else: 
                    // console.log('discarding point', point);
                }
            });



            // console.log('AddPoints returning', collectionLength !== collection.length, collection.length);
            return collectionLength !== collection.length;
        };

    }

    // I want to factor out all DOM code in Device class.
    function Device(device) {
        // Privates
        var context = this,
            horizontalSlider,
            graphicsEventHandler;

        this.devicetitle = device.devicetitle;
        this.id = device.id;
        this.imei = device.imei;
        this.lastpos = device.lastpos;
        this.username = device.username;

        this.RGB = device.RGB;
        this.color = rgbToColor(device.RGB, 0.6);

        this.getIconColor = function () {
            if (this.isActive()) {
                return this.color;
            }
            return new Color('gray');
        };
        this.getPauseIconColor = function () {
            return this.color;
        };

        this.height = device.height;
        this.url = device.url;
        this.width = device.width;

        this.line = new PointCollection();

        this.graphics = new GraphicsLayer();

        this.setNewGraphicsEventHandler = function (obj) {
            if (graphicsEventHandler) {
                graphicsEventHandler.remove();
            }
            graphicsEventHandler = obj;
        };


        this.poll = {
            failed: 0,
            interval: 2300,
            getData: function () {

                var line_url = "/api/line/" + context.id + "/" + (context.sliderValue || 2);
                // No idea why ..
                // It seems that mtConfig.lineService ALWAYS is undefiend .. remove
                if (mtConfig.lineService !== undefined && !dojo.isIE) {
                    line_url = mtConfig.lineService;
                }
                if (dojo.byId("visible" + context.id) && dojo.byId("visible" + context.id).checked) {
                    dojo.xhrGet({
                        url: line_url,
                        timeout: 30000,
                        handleAs: 'json',
                        load: function (result) {
                            if (result.paths instanceof Array) {
                                context.line.addPoints(result);
                                drawRoute(result, context);
                                context.poll.init();
                                if (context.poll.failed > 0) {
                                    context.poll.failed -= 1;
                                }
                            } else {
                                context.poll.errorHandler();
                            }
                        },
                        error: function (error, ioargs) {
                            console.error(error);
                            context.poll.errorHandler(ioargs);
                        },
                        failOk: true
                    });
                } else {
                    context.graphics.clear();
                    context.poll.init();
                }
            },
            errorHandler: function (arg) {
                if (arg.xhr.status === 503) {
                    console.log('Backends are down!');
                    throw new Error('Backends are down (503)');
                }
                context.poll.failed += 1;
                if (context.poll.failed < 10) {
                    context.poll.init();
                } else {
                    console.error('Nógvir feilir við eind ' + context.id);
                    window.location.reload();
                }
            },
            init: function () {
                window.setTimeout(context.poll.getData, (context.poll.interval + 2000 * context.poll.failed));
                // console.log("Checking if device", context.devicetitle, "is active: ", context.isActive());
            }
        };

        this.addSlider = function () {
            require(["dijit/form/HorizontalSlider"], function (HorizontalSlider) {
                horizontalSlider = new HorizontalSlider({
                    name: "slider" + context.id,
                    value: 2,
                    minimum: mtConfig.slider.minValue,
                    maximum: mtConfig.slider.maxValue,
                    intermediataChanges: true,
                    style: "width: 200px; float: right;",
                    onChange: function (value) {
                        context.sliderValue = value;
                    }
                }, "slider" + device.id);
            });
        };

        /**
         * IsActive returns true, the unit has updated position within the last 15 minutes
         */
        this.isActive = function () {
            return this.line.getLastPoint() && // Has a point
                new Date() - this.line.getLastPoint().timestamp < 15 * 60 * 1000; // not older than 15 minutes
        };


        /**
        setInterval(function () {
            console.log("Reporting service for device", context.devicetitle);
            console.log("    isActive(): ", context.isActive());
        }, 1000 + Math.random() * 3500);
        */
    }

    /**
     * Get_devices heintar ein lista av eindum
     * og ger devices úr tí listanum
     * Knýtur í socket.io
     *
     *
     * Hendan funkan verður kallað einaferð úr init()
     */
    function get_devices() {

        require(["//wstracker.kort.fo/socket.io/socket.io.js"], function (io) {
            socket = io.connect('//wstracker.kort.fo');

            $(document).trigger('socket_ready');
        });

        require(["dojo/_base/xhr"], function (xhr) {
            xhr.get({
                url: "get_devices.php",
                handleAs: 'json'
            }).then(function (devices) {
                devices.forEach(function (deviceObj) {
                    var device = new Device(deviceObj);
                    mtData.devices.push(device);
                    device.poll.init();
                    map.addLayer(device.graphics);

                    displayDevice(device);
                });


                $(document).trigger('colors_ready');
                console.log("trigger colors_ready!");

            }, function (err) {
                console.error(err);
                window.setTimeout(function () {
                    console.error("RELOAD - errors");
                    // window.location.reload();
                }, 5000);
            });
        });
    }


    function init() {
        var extentChangeSave = {
            save: function (extent) {
                console.log("Saving extent...");
                if (extent.hasOwnProperty('xmax')) {
                    require(['dojo/_base/xhr'], function (xhr) {
                        xhr.get({
                            url: '/tracking/saveExtent.php',
                            content: {
                                type: 'extent',
                                xmax: extent.xmax,
                                xmin: extent.xmin,
                                ymax: extent.ymax,
                                ymin: extent.ymin
                            }
                        });
                    });
                }
            }
        };

        mtData.flasher.init();

        get_devices();
        require(['esri/map', 'esri/layers/ArcGISTiledMapServiceLayer', 'esri/layers/ArcGISDynamicMapServiceLayer', 'esri/geometry/Extent'], function (Map, Tiled, Dynamic, Extent) {
            map = new Map("map", { logo: false, extent: new Extent(mtConfig.initialExtent)});
            document.getElementById('map').style.height = "100%";
            map.resize();

            // Here I should really just use webmaps instead.
            array.forEach(mtConfig.maps, function (layer) {
                var layerTypeMap = {
                    ArcGISTiledMapServiceLayer: Tiled,
                    ArcGISDynamicMapServiceLayer: Dynamic
                };
                map.addLayer(new layerTypeMap[layer.type](layer.url));
            });

            on(map, "ExtentChange", function () {
                pxWidth = map.extent.getWidth() / map.width;
                if (extentChangeSave.hasOwnProperty('timer')) {
                    clearTimeout(extentChangeSave.timer);
                }
                extentChangeSave.timer = window.setTimeout(function () {
                    extentChangeSave.save(map.extent);
                }, 2000);
            });

        });

        // Adressuleitan her
        if (mtConfig.addressSearch === true) {
            require(['/assets/munin/adressuleitan/module.js'], function (SearchModule) {
                var searchModule = new SearchModule();
                $('.icon-search').click(function () {
                    $('#addressSearch').show();
                    $('#addressSearch input').focus();
                    $('#addressSearch').append(function () {
                        return $('<span class="addressClose">\'</span>').click(function () {
                            if (mtData.hasOwnProperty('addressSearchGraphic')) {
                                map.graphics.clear(mtData.addressSearchGraphic);
                            }
                            $(this.parentNode).hide();
                            $(this).remove();
                        });
                    });
                });

                $('input.typeahead').livequery(function () {
                    $(this).typeahead(searchModule.typeahead);
                });

            });
            $('#addressSearch').bind('foundAddress', function (e, address) {
                if (address.hasOwnProperty('geometry')) {
                    if (mtData.hasOwnProperty('addressSearchGraphic')) {
                        map.graphics.clear(mtData.addressSearchGraphic);
                    }

                    map.centerAt(address.geometry);
                    require(['esri/graphic', 'esri/geometry/Point', 'esri/symbols/SimpleMarkerSymbol'], function (Graphic, Point, SimpleMarkerSymbol) {
                        mtData.addressSearchGraphic = new Graphic(
                            new Point(address.geometry.x, address.geometry.y, map.spatialReference),
                            new SimpleMarkerSymbol()
                        );
                        map.graphics.add(mtData.addressSearchGraphic);
                    });
                }
            });
        }

        on(window, 'resize', function () {
            clearTimeout(resizeTimer);
            resizeTimer = window.setTimeout(function () {
                map.resize();
            }, 200);
        });


        /**
         * Button to return to initial extent.
         */
        $('.icon-globe').click(function () {
            map.setExtent(mtConfig.initialExtent);
        });

        /**
         * Button to toggle sidebar
         */
        $('#showOrHideSidebar').click(function () {
            $('#showOrHideSidebar').toggleClass('icon-chevron-right icon-chevron-left');
            $('#admin').toggleClass('collapsed');
        });

    } // End function init

    // Koyr init() tá dom er ready
    if (!mtConfig.hasOwnProperty('noInit')) {
        require(["dojo/domReady!"], function () {
            init();
        });
    }

    /***
     * Kontekst menu til at síggja status rapportir fyri eindir.
     */
    require(["dijit/registry", "dijit/Menu", "dijit/MenuItem", "dijit/Dialog", "dojo/query!css2"], function (registry, Menu, MenuItem, Dialog) {
        var menu = new Menu({
            targetNodeIds: ["admin"],
            selector: ".unit"
        });
        menu.addChild(new MenuItem({
            label: 'Vís status rapport',
            onClick: function (evt) {
                var node = this.getParent().currentTarget,
                    deviceId = domAttr.get(node, 'deviceid');

                $.get('/api/device/status/' + deviceId, function (data) {
                    var myDialog = new Dialog({
                        title: 'Status boð fyri eind#' + deviceId,
                        content: data,
                        style: "width: 800px;"
                    });
                    myDialog.show();
                });


            }
        }));
    });
});

