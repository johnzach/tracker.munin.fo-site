/*global console, $, moment */
// GLOBAL VARIABLESE

var elem = function (elem) {
        "use strict";
        return function (child) {
            return $('<' + elem + ' />').append(child);
        };
    },
    $th = elem('th'),
    $tr = elem('tr'),
    $td = elem('td');

function constructTaskStatusTable(task) {
    "use strict";
    var $tableRow = $tr().addClass('stempling ' + task.id),
        $tableRowData = $td().attr('colspan', '7'),
        $stemplingTable = $('<table />').addClass('stemplingTable'),
        $stemplingTHead = $('<thead />'),
        $stemplingTBody = $('<tbody />'),
        $stemplingTheadTr = $('<tr />'),
        $date = $th('Dagfesting'),
        $comment = $th('Frágreiðing'),
        $statusDesc = $th('Status');

    $stemplingTheadTr.append($date, $comment, $statusDesc);
    $stemplingTHead.append($stemplingTheadTr);
    $stemplingTable.append($stemplingTHead, $stemplingTBody);

    $.each(task.statuses, function (i, stempling) {
        var $tr = $('<tr />'),
            $date = $td(moment(stempling.datetime).lang('fo').format('ll H:mm'));

        $tr.append($date, $td(stempling.comment), $td(stempling.userStatus.description));
        $stemplingTBody.append($tr);
    });

    $tableRowData.append($stemplingTable);
    $tableRow.append($tableRowData);

    return $tableRow;
}

function buildTable(dataIn) {
    "use strict";
	console.log("Building table.....", dataIn);

	var openSymbol = "glyphicon-chevron-down",
	    closeSymbol = "glyphicon-chevron-up",
        $taskTable = $('<table />'),
        $taskTableTHead = $('<thead />'),
        $taskTableTBody = $('<tbody />'),
        $taskTableTHeadTr = $tr();

    $taskTable
        .addClass('tablesorter-blue table table-hover table-condensed tablesoerter')
        .attr('id', 'taskTable')
        .append($taskTableTHead, $taskTableTBody);

    $taskTableTHead.append($taskTableTHeadTr);

    $taskTableTHeadTr.append(
        $th(),
        $th('ID'),
        $th('Dagfesting'),
        $th('Frágreiðing'),
        $th('Funkupottur'),
        $th('Stovnað av'),
        $th('Støða')
    );

	$.each(dataIn.tasks, function (i, task) {
        var $row = $tr().attr('value', task.id),
            $icon = $('<span />').addClass('glyphicon ' + openSymbol).css('font-size', '0.8em').hide(),
            $date = moment(task.created).lang('fo').format('ll H:mm'),
            $statusTable = constructTaskStatusTable(task),
            latestStatus = $('<span>Stovnað</span>');

        if (moment().diff(moment(task.created), 'days') > 31) {
            $row.addClass('task-late');
        }

        $row.append(
            $td($icon),
            $td(task.id),
            $td($date),
            $td(task.description),
            $td(task.funku_pottur.description),
            $td(task.created_by),
            $td(latestStatus)
        );
        $taskTableTBody.append($row);

		// Log stemplingar
		if (task.statuses.length > 0) {
            $icon.show();
            latestStatus.text(task.statuses[task.statuses.length - 1].userStatus.description);
            if (task.statuses[task.statuses.length - 1].userStatus.description === 'Liðugt') {
                $row.addClass('task-done');
                $row.removeClass('task-late');
            }


            $statusTable.hide();
            $row.click(function () {
                $statusTable
                    .toggle()
                    .insertAfter($row);

                $icon
                    .toggleClass(openSymbol)
                    .toggleClass(closeSymbol);
            });
        }

	}); /*END for each task*/


	// Set table to report page
	$("#yvirlitDiv").html($taskTable);

    $("#taskTable")
        .tablesorter()
        .bind('sortStart', function () {
            console.log("Collapse all rows here, before sorting");
            $('.stempling:visible').each(function (i, stempling) {
                var $stempling = $(stempling);
                $stempling.hide();
                $stempling.prev().find('.glyphicon')
                    .toggleClass(openSymbol)
                    .toggleClass(closeSymbol);
            });
        });

}



// Functions downloads the data from the server in JSON object---------------------------------------------

function heintaData(user) {
    "use strict";

	// var jsonUrl = "js/data.js";
	var jsonUrl = "http://tracker.munin.fo/api/tasks_new.json",
        data = {
            token: '46Xkhujpe6LikU6axtqlB1UUV/1mdmoqOJZRl2pIXjST8yGtYWTJfrTMjgnHsteF'
        };

    if (user) {
        data.task_user_id = user.id;
    }

	$.getJSON(jsonUrl, data)

	// Once data are downloaded continue generation of the report
        .done(function (data) {
            buildTable(data);
        })

        .fail(function (e) {
            console.log("Data couldn't be loaded");
            console.warn(e.statusText);
        })

        .always(function () {
            console.log("Download is finished....");
        });
}
// Function process the data from server to html table ----------------------------------------------------

function option(data, value) {
    "use strict";
    return $('<option value="' + value + '">' + data + "</option>");
}

function addUsersToDropdown(users) {
    "use strict";
    var $sel = $('#employees');
    users.forEach(function (user) {
        $sel.append(option(user.user_name + " - " + user.org_section.name, user.id));
    });
    $sel.change(function () {
        var id = $(this).val(),
            user = users.filter(function (user) {
                return user.id === id;
            })[0];

        heintaData(user);
    });
}

// loads on pageload
function init() {
    "use strict";
	// Get JSON object from the service
	var data = heintaData();

    // Fetch users
    $.get('/api/tasks/users.json').done(function (users) {
        addUsersToDropdown(users);
    });

    $('#show_finished_tasks').click(function () {
        $('.task-done').toggle();
    });
}
