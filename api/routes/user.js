/*global module, require */
/** @module routes/user */

module.exports = function (app, client) {
    "use strict";
    var url = require('url');

    /**
     * Returns userId from token.
     * If invalid token, it returns a error with HTTP code 400
     */
    app.get('/api/user', function (req, res) {
        var urlQuery = url.parse(req.url, true).query;

        // Token is in urlQuery.token
        //
        // Lookup in devicemodel

        client.query(
            'select user_id from tokens where token = $1',
            [urlQuery.token],
            function (err, result) {
                if (err || result.rows.length < 1) {
                    res.send(400, JSON.stringify({
                        error: 'Not valid token.'
                    }));
                    return;
                }
                res.end(JSON.stringify({
                    user_id: result.rows[0].user_id
                }));

            }
        );
    });
};
