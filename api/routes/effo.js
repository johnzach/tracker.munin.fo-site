/*global module, console*/
/** @module routes/effo */


/**
 * Holds the routes related to the Effo project
 *
 * @constructor
 */
function EffoRoute(app) {
    "use strict";

    /**
     * Method to receive and insert event file.
     *
     * The structure of the event file is as follows:
     *
     *
     * @param {Request} req - ExpressJS Request
     * @param {Resource} res - ExpressJS Response
     */
    function postEvent(req, res) {
        console.log(req, res);
    }

    /**
     * Parses and inserts a delivery note
     * @param req
     * @param res
     */
    function postDeliveryNote(req, res) {
        console.log(req, res);
    }

    /**
     * Returns a deliveryNote in PDF format.
     *
     * req.param.deliverNoteNumber is the reference to the note
     *
     * @param req
     * @param res
     */
    function getDeliveryNote(req, res) {
        console.log(req, res);
    }


    /**
     * Returns a list of deliveries by address.
     *
     * @param req
     * @param res
     */
    function getDeliveriesByAddress(req, res) {
        console.log(req, res);
    }


    /** Install routes */
    app.post('/api/effo/event', postEvent);
    app.post('/api/effo/delivery', postDeliveryNote);
    app.get('/api/effo/adressa/:addressIdentifier', getDeliveriesByAddress);
    app.get('/api/effo/deliveryNote/:deliveryNoteNumber', getDeliveryNote);

}


module.exports = EffoRoute;

