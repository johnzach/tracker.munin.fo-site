module.exports = function (app) {
    "use strict";
    app.get('/api', function (req, res) {

        if (req.session === undefined) {
            res.redirect('/sessionCheck/');
        }
        res.render(__dirname + '/../views/index.jade');
    });
}
