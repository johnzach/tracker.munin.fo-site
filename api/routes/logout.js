module.exports = function (app) {
    "use strict";
    app.get('/api/logout', function (req, res) {
        if (req.session !== undefined) {
            req.session.destroy();
        }
        res.redirect('/');
    });
}
