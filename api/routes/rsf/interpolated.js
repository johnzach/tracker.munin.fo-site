module.exports = function (app, client) {
    /* Haldi ikki at hetta verður nýtt meir - lokal extrapolering verður nýtt í staðin */
    "use strict";
    app.get('/rsf/interpolated/:stevna/:rodur', function (req, res) {
        var query = client.query(
            'SELECT  \
    d.device_id, \
    st_x(d.p) x, \
    st_y(d.p) y, \
    s.rgb, \
    d.navn, \
    d.kvf \
    FROM ( \
    SELECT  \
    device_id, \
    navn, \
    kvf, \
    CASE  \
        WHEN dt > interval \'0\' THEN  \
            ST_Line_Interpolate_Point(ST_MakeLine(( \
                SELECT \
                    st_astext(sub.the_geom) \
                FROM "Tracking" sub \
                WHERE sub."DeviceID" = device_id \
                    AND sub."TimeStamp" < ts - dt \
                ORDER BY sub."TimeStamp" DESC \
                LIMIT 1 \
            ), ( \
                SELECT \
                    st_astext(sub.the_geom) \
                FROM "Tracking" sub \
                WHERE sub."DeviceID" = device_id \
                   AND sub."TimeStamp" >= ts - dt \
                ORDER BY sub."TimeStamp" ASC \
                LIMIT 1 \
            ) \
        ),  \
        date_part(\'epoch\', ( \
                (ts - dt) - ( \
                SELECT \
                    "TimeStamp"  \
                FROM "Tracking" sub \
                WHERE sub."DeviceID" = device_id \
                   AND sub."TimeStamp" < ts - dt \
                ORDER BY sub."TimeStamp" DESC \
                LIMIT 1) \
            )) / date_part(\'epoch\',  \
                ( \
                    SELECT \
                        "TimeStamp"  \
                    FROM "Tracking" sub \
                    WHERE sub."DeviceID" = device_id \
                       AND sub."TimeStamp" >= ts - dt \
                    ORDER BY sub."TimeStamp" ASC \
                    LIMIT 1 \
                ) - ( \
                    SELECT \
                        "TimeStamp" \
                    FROM "Tracking" sub \
                    WHERE sub."DeviceID" = device_id \
                        AND sub."TimeStamp" < ts - dt \
                    ORDER BY sub."TimeStamp" DESC \
                    LIMIT 1 \
                ) \
            )) \
        ELSE the_geom \
    END AS p \
    FROM ( \
    SELECT  \
       t."DeviceID" device_id \
      , navn \
      , kvf \
      , t."TimeStamp" ts \
      , t.the_geom \
      , (t."TimeStamp" - min(t."TimeStamp") OVER()) dt \
    FROM ( \
        SELECT \
            "DeviceID", \
            navn, \
            kvf, \
            max("TimeStamp") AS "TimeStamp" \
        FROM \
            "Tracking", ( \
            SELECT \
                d."ID" AS id, \
                rsf_batar.navn, \
                rsf_batar.kvf \
            FROM  \
                "Device" AS d  \
                INNER JOIN rsf_batar  \
                    ON rsf_batar.device_id = d."ID" \
                INNER JOIN rsf_batar_rodur_rel AS rel \
                    ON rel.batur_id = rsf_batar.id \
                INNER JOIN rsf_rodur \
                    ON rel.rodur_id = rsf_rodur.id \
                INNER JOIN rsf_stevna \
                    ON rsf_rodur.stevna_id = rsf_stevna.id \
            WHERE 1=1 \
                AND rel.rodur_id = rsf_rodur.id \
                AND rel.batur_id = rsf_batar.id \
                AND rsf_rodur.navn  = \'Fimmmannafør, dreingir\' \
                AND rsf_stevna.navn = \'Sundalagsstevna\' \
            )  \
            AS rsf_rel  \
        WHERE 1=1 \
            AND rsf_rel.id = "DeviceID"  \
            AND "TimeStamp" > NOW() - interval \'8 seconds\' \
        GROUP BY \
            "DeviceID", navn, kvf) as x  \
        INNER JOIN "Tracking" AS t  \
            ON t."DeviceID" = x."DeviceID"  \
                AND t."TimeStamp" = x."TimeStamp" \
    WHERE 1=1 \
    ) rel \
    WHERE 1=1 \
    ) as d INNER JOIN ( \
    SELECT \
    s."RGB" rgb, \
    "Device"."Name" devicetitle, \
    "Device"."ID" device_id \
    FROM "Symbology" s INNER JOIN "Device" ON "Device"."SymbologyID" = s."ID" \
    ) s ON s.device_id = d.device_id'
        ),
            devices = [];
        query.on('row', function (row) {
            devices.push(row);
        });
        query.on('end', function () {
            res.send(JSON.stringify(devices));
        });

    });
}
