/*jslint nomen: true*/
/*global __dirname, require, console, setInterval */
var application_root = __dirname,
    express = require('express'),
    http = require('http'),
    async = require('async'),
    colors = require('colors'),
    pg = require('pg'),
    path = require('path'),
    events = require('events'),
    emitter = new events.EventEmitter(),
    app = express(),
    server = http.createServer(app),
    mail = require('nodemailer').mail,
    requests = 0,
    androidSockets = {
        open: []
    },
    arcgis_token = {
        data: {}
    };

// Database
var client = new pg.Client('tcp://postgres@localhost/tracking');
client.connect();

// Configure the application
require('./config/config.js')(app, express);

app.all('*', function (req, res, next) {
    "use strict";
    res.removeHeader("X-Powered-By");
    console.log(req.headers['x-forwarded-for'] + ": ", req.path);

    requests += 1;
    next();
});

// Order matters
require('./routes/session.js')(app, client);
require('./routes/line.js')(app, client);
require('./routes/tasks.js')(app, client, emitter);
require('./routes/devices.js')(app, client, androidSockets);
require('./routes/user.js')(app, client);
require('./routes/rsf/interpolated.js')(app, client);
require('./routes/rsf/stevna.js')(app, client);
require('./routes/logout.js')(app);
require('./routes/index.js')(app);

// Receive points
require('./recv/android.js')(client, androidSockets, emitter);


// socket.io verður implementera seinni, bert til test..
require('./io/io.js')(app, client, emitter);

// Launch server
app.listen(8000);

/**
 * On API Exception the system will email the system administrators,
 * and alert them that something is "gruelig-galt" (da)
 */
emitter.on('api_exception', function (err) {
    "use strict";

    mail({
        from: 'Error reporting <no-reply@munin.fo>',
        to: 'rj@munin.fo',
        subject: 'Tracking API Exception',
        text: '',
        html: "<pre>" + JSON.stringify(err, 0, 4) + "</pre>"
    });

});

emitter.on('error', function (err) {
    "use strict";
    err.res.statusCode = err.statusCode || 400;
    err.res.end(JSON.stringify({error: err.message}, 4, 4));
});

emitter.on('token_error', function (err) {
    "use strict";
    
    err.res.statusCode = err.statusCode || 401;
    err.res.end(JSON.stringify({error: 'Token error'}, 4, 4));
});

emitter.on('arcgis_token_update', function (param_arcgis_token) {
    "use strict";
    arcgis_token.age = new Date();
    arcgis_token.data = param_arcgis_token;

    /*
    mail({
        from: 'token mgr <no-reply@munin.fo>',
        to: 'rj@munin.fo',
        subject: 'token update',
        text: '',
        html: "<pre>" + JSON.stringify(arcgis_token, 0, 4) + "</pre>"
    });
    */
});

/**
 * Droppa console log, ger heldur ein pr. hour email log
 */

var serviceStartTime = new Date(),
    tag = function (tag) {
        "use strict";
        return function (contents) {
            return "<" + tag + ">" + contents + "</" + tag + ">";
        };
    },
    td = tag('td'),
    pre = tag('pre'),
    th = tag('th'),
    tr = tag('tr');

function sendStatusReport() {
    "use strict";
    var content = [];

    content.push('<h1>Yvirlit yvir tracking skipan</h1>');

    content.push('<table>');
    content.push(tr(td('Tænastan er starta') + td(serviceStartTime)));
    content.push(tr(td('Tal av íbundnum eindum') + td(androidSockets.open.length)));
    content.push(tr(td('Tal av HTTP fyrispurningum síðan start') + td(requests)));
    content.push(tr(td('Arcgis server token age') + td(arcgis_token.age)));
    content.push(tr(td('Arcgis server token') + td(pre(JSON.stringify(arcgis_token.data, 0, 4)))));
    content.push('</table>');

    content.push('<h1>Yvirlit yvir tilknýttar eindir</h1>');
    content.push('<table>');
    content.push(tr(th('Eind') + th('IP') + th('Punkt') + th('Seinasta punkt')));
    androidSockets.open.forEach(function (trackingDevice) {
        content.push(tr(td(trackingDevice.id || "Ókend eind") + td(trackingDevice.ip) + td(trackingDevice.insertedPoints) + td(new Date(trackingDevice.lastPoint * 1000))));
        console.log(trackingDevice);
    });
    content.push('</table>');


    mail({
        from: 'Tracking reporter <no-reply@munin.fo>',
        to: 'rj@munin.fo',
        subject: 'Trackingskipan, yvirlit',
        text: '',
        html: content.join('<br />')
    });

}

sendStatusReport();
setInterval(sendStatusReport, 1000 * 60 * 60 * 2);

/**
var now = new Date();

function serviceStatus() {
    "use strict";
    var i;
    console.log('\u001B[2J\u001B[0;0f'); // Clear screen and set cursor to 0,0
    console.log(('  Uptime: ' + Math.floor((new Date() - now) / 1000) + " seconds").cyan);

    console.log();

    console.log(('  Number of HTTP requests served: ' + requests).blue);
    console.log('  Android clients currently connected: '.green);
    for (i = 0; i < androidSockets.open.length; i += 1) {
        console.log(('    ' + (androidSockets.open[i].id ||
            ('Unknown unit, stack: ' + androidSockets.open[i].pointStack.length + ' items')) +
            ' - ' + androidSockets.open[i].ip + ' - ' +
            androidSockets.open[i].insertedPoints + ' points, last:' +
            new Date(androidSockets.open[i].lastPoint * 1000)).yellow);
    }
}

setInterval(serviceStatus, 2000);
*/
