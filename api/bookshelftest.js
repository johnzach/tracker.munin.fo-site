/*jslint vars: true */
/*global require, console, FunkuPottur */

function printModel(model) {
    "use strict";
    console.log(JSON.stringify(model, 0, 4));
}


(function () {
    "use strict";
    var Bookshelf = require('bookshelf'),
        ArcgisLib = require('../triggers/ArcgisLib'),
        arcgisLib = new ArcgisLib({
            servername: 'srv8.kort.fo'
        });


    /**
    arcgisLib.setToken('thr', 'tkai');
    var torshavnar_aarinntok = arcgisLib.getFeatureLayer('torshavnar/torshavnar_aarinntok/FeatureServer/0/'),
        inntakPromise = torshavnar_aarinntok.getFeature({upprid: 3});

    inntakPromise.then(function (inntak) {
        printModel(inntak);
    });
    */

    Bookshelf.PG = Bookshelf.initialize({
        client: 'pg',
        connection: {
            host: 'localhost',
            user: 'postgres',
            database: 'tracking'
        }
    });


    var a = 1,

        /**
         * Models
         */
        User = Bookshelf.PG.Model.extend({
            tableName: 'User',
            idAttribute: 'ID'
        }),

        Symbology = Bookshelf.PG.Model.extend({
            tableName: 'Symbology',
            idAttribute: 'ID'
        }),

        Device = Bookshelf.PG.Model.extend({
            tableName: 'Device',
            idAttribute: 'ID',
            user: function () {
                return this.belongsTo(User, 'UserID');
            },
            symbology: function () {
                return this.belongsTo(Symbology, 'SymbologyID');
            }
        }),

        Tracking = Bookshelf.PG.Model.extend({
            tableName: 'Tracking',
            idAttribute: 'ID',
            device: function () {
                return this.belongsTo(Device, 'DeviceID');
            }
        }),

        TaskDevice = Bookshelf.PG.Model.extend({
            tableName: 'task_devices'
        }),
        TaskLocation = Bookshelf.PG.Model.extend({
            tableName: 'task_location'
        }),
        TaskUserStatus = Bookshelf.PG.Model.extend({
            tableName: 'task_user_status'
        }),
        TaskStatus = Bookshelf.PG.Model.extend({
            tableName: 'task_status',
            userStatus: function () {
                return this.belongsTo(TaskUserStatus);
            }
        }),

        ArcgisIdentifier = Bookshelf.PG.Model.extend({
            tableName: 'arcgis_identifier'
        }),

        Task = Bookshelf.PG.Model.extend({
            tableName: 'task',
            user: function () {
                return this.belongsTo(User, 'user_id');
            },
            devices: function () {
                return this.hasMany(TaskDevice);
            },
            locations: function () {
                return this.hasMany(TaskLocation);
            },
            statuses: function () {
                return this.hasMany(TaskStatus);
            },
            funku_pottur: function () {
                return this.belongsTo(FunkuPottur, 'funkupottur');
            },
            arcgis_identifier_obj: function () {
                return this.belongsTo(ArcgisIdentifier, 'arcgis_identifier');
            }

        }),

        TaskUser = Bookshelf.PG.Model.extend({
            tableName: 'task_user'
        }),

        OrgSection = Bookshelf.PG.Model.extend({
            tableName: 'org_section',
            children: function () {
                return this.hasMany(OrgSection, 'parent');
            },
            employees: function () {
                return this.hasMany(TaskUser, 'org_section');
            },
            leader: function () {
                return this.belongsTo(TaskUser, 'leader');
            }
        }),
        TaskFunkuPotturUserRel = Bookshelf.PG.Model.extend({
            tableName: 'task_funku_pottur_users_rel',
            idAttribute: 'task_user_id'

        }),
        TaskFunkupotturRouter = Bookshelf.PG.Model.extend({
            tableName: 'task_funkupottur_router'
        }),
        FunkuPottur = Bookshelf.PG.Model.extend({
            tableName: 'tasks_funku_pottur',
            users: function () {
                return this.hasMany(TaskUser, 'task_funku_pottur_id').through(TaskFunkuPotturUserRel, 'id');
            },
            org_section: function () {
                return this.belongsTo(OrgSection);
            },
            routerEntries: function () {
                return this.hasMany(TaskFunkupotturRouter, 'tasks_funkupottur');
            }
        }),
        Tasks = Bookshelf.PG.Collection.extend({
            model: Task
        });


    var fp1 = new FunkuPottur({id: 1})
            .fetch({withRelated: ['users', 'org_section.employees', 'org_section.employees', 'org_section.leader']})
            .then(),

        fp2 = new FunkuPottur({id: 2})
            .fetch({withRelated: ['users', 'org_section.employees', 'org_section.employees', 'org_section.leader']})
            .then(),

        fp3 = new TaskFunkupotturRouter({task_type: 'umvaelast', task_value: 'ja'})
            .fetch()
            .then(),

        aid = new ArcgisIdentifier({
            featureclass: 'torshavnar_aarinntok_uz',
            orig_id: '18593'
        }),

        tasks = Task
            .collection()
            .query(function (qb) {
                qb.where('deleted', 'is', 'null');
                qb.where('user_id', '=', 361);
            })
            .fetch({withRelated: [
                'devices',
                'arcgis_identifier_obj',
                'statuses.userStatus'
            ]})
            .then(function (collection) {
                // Columns to filter by:
                // funkupottur
                // arcgis_identifier
                // orig_id 14874

                var filteredCollection = collection
                    .filter(function FilterOutEndedTasks(task) {
                        var statuses = task.related("statuses").filter(function (taskStatus) {
                            return (taskStatus.related("userStatus").get("system_status") !== 'END');
                        });

                        // Keep record if no "END" status is found.
                        return statuses.length === task.related("statuses").length;
                    })
                    .filter(function FilterOutTasksByArcgisIdentifier(task) {
                        return (task.related("arcgis_identifier_obj").get('orig_id') === "14874");
                    })
                    .filter(function FilterOutByFunkupott(task) {
                        return task.get('funkupottur') === "2";
                    });

                printModel(filteredCollection);

            });



/*    aid.fetch()
        .then(function (model) {
            if (model === null) {
                aid
                    .save()
                    .then(printModel);
            } else {
                printModel(model);
            }
        })
        .catch(function (e) {
            console.log(e);
        });
        */
            // .save({}, {patch: true})
            // .then(printModel);


        /*
        tasks = new Task({id: 632})
            // .collection()
            .query(function (qb) {
                qb.where('deleted', 'is', null);
                qb.where('user_id', '=', 361);
            //  qb.join('tasks_funku_pottur', 'tasks_funku_pottur.id', '=', 'task.funkupottur');
            //  qb.join('task_funku_pottur_users_rel', 'task_funku_pottur_users_rel.task_funku_pottur_id', '=', 'tasks_funku_pottur.id');
            //  qb.join('task_user', 'task_user.id', '=', 'task_funku_pottur_users_rel.task_user_id');
            //  qb.where('task_user.id', '=', '23');
            })
            .fetch({withRelated: [
                'devices',
                'locations',
                'statuses',
                'funkupottur_obj.users'
            ]})
            .then(),
        taskT1 = new Task({id: 1244})
                .fetch({withRelated:
                [
                    'statuses.userStatus',
                    'arcgis_identifier_obj',
                    'funkupottur_obj.routerEntries'
                ]})
                .then(
                function (taskModel) {
                    console.log(taskModel.related('funkupottur_obj').related('routerEntries').at(0).get('task_type'));


            

                    // printModel(taskModel);
                }
            );

                 * function (taskModel) {
                var tus1 = new TaskUserStatus({
                        user_id: taskModel.get('user_id'),
                        system_status: 'END'
                    })
                        .fetch()
                        .then(function (taskUserStatus) {
                            var ts1 = new TaskStatus({
                                task_id: taskModel.get('id'),
                                datetime: 'NOW()',
                                task_user_status_id: taskUserStatus.get('id'),
                                comment: 'By API'
                            });
                            ts1.save();
                        });



            });


        arcgisId = ArcgisIdentifier
            .collection()
            .query(function (qb) {
                qb.where("featureclass", '=', "torshavnar_aarinntok_uz");
                qb.where("orig_id", "=",  18472);
            })
            .fetch()
            .then(function (result) {
                var ags_ids = result.pluck('id');
                                                                                                   
                Task
                    .collection()
                    .query(function (qb) {
                        qb.whereIn('arcgis_identifier', ags_ids);
                        qb.where('funkupottur', '=', 1);
                    })
                    .fetch({withRelated: ['statuses.userStatus']})
                    .then(function (tasks_to_be_marked_as_complete) {
                        tasks_to_be_marked_as_complete = tasks_to_be_marked_as_complete.filter(function (task) {
                            if (task.get('deleted')) {
                                console.log("dropping task, is deleted.");
                                return false;
                            }

                            var taskParsed = JSON.parse(JSON.stringify(task)),
                                isComplete = (taskParsed.statuses.filter(function (taskStatus) {
                                    return (taskStatus.userStatus.system_status === 'END');
                                }).length === 1);

                            console.log('Mark as complete', task.get('id'), 'funkupottur', task.get('funkupottur'), 'is complete: ', isComplete);

                            return !isComplete;
                            
                        });

                        console.log(JSON.stringify(tasks_to_be_marked_as_complete, 0, 4));

                        // console.log('Task ids, to be marked complete: ', result.pluck('id'));
                    });
            });

*/


/*
        users = TaskUser.collection().fetch().then(),

        tasks = Task.collection().query(function (qb) {
            qb.where('deleted', 'is', 'null');
            qb.where('user_id', '=', 31);
        }).fetch({withRelated: [
            'devices',
            'locations',
            'statuses'
        ]}).then(printModel);

*/

        // Roynd at avmynda kommunuhierarki
        // In-parameter: topNode
        //
        // TK, topNode: 1

    /**
    var teknisk_deild = new OrgSection({id: 1})
        .fetch({withRelated: ['children.employees', 'children.children.employees', 'employees']})
        .then(printModel);
*/

        /**
         *  Test functions
         */

        /**
        s4 = new Device({'ID': 187})
            .fetch({withRelated: ['user', 'symbology']})
            .then(printModel),


        latestPoint = new Tracking({'ID': '106733365'})
            .fetch({withRelated: ['device']})
            .then(printModel),

        task1 = new Task({'id': 428})
            .fetch({withRelated: ['user', 'devices', 'locations', 'statuses']})
            .then(printModel);

        */
}());
