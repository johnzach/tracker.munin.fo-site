/*global module*/

module.exports = function (grunt) {
    "use strict";
    grunt.initConfig({
        jsdoc: {
            dist: {
                src: ['models/*.js', 'routes/*.js', 'api.js'],
                options: {
                    destination: '../www/doc'
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-jsdoc');
    grunt.registerTask('default', ['jsdoc']);

}
