#!/usr/bin/env node

    var http = require('http'),
        url = require('url'),
        pg = require('pg'),
        util = require('util'),
        totalUpdates = 0,
        totalRequests = 0,
        startTime = new Date();


    var devices = {},
        time = 40;

    var client = new pg.Client("tcp://postgres@localhost/tracking");
    client.connect();

    function fetch(ID) {
        var whereclause = '', sql;
        if (devices[ID].hasOwnProperty('paths') && devices[ID].paths[0].length > 0) {
            whereclause = ' AND "ID" > ' + devices[ID].paths[0][0][3];
        }

        sql = 'SELECT "ID", st_x(the_geom), st_y(the_geom), to_char("TimeStamp", \'MM/DD/YYYY HH24:MI:SS\') as ts, "DeviceID", speed, accuracy FROM "Tracking" WHERE "DeviceID" = ' + ID + whereclause + ' ORDER BY "TimeStamp" DESC LIMIT 2 ';
        client.query(sql, function(err, result) {
            if (err) {
                console.log(err);
                return;
            }
            if (result.rows.length > 0) {
                var paths = [];
                devices[ID].paths = [];
                result.rows.forEach(function(row) {
                    var path = [row.st_x, row.st_y, row.ts, row.ID, row.speed, row.accuracy];
                    paths.push(path);
                });

                devices[ID] = {
                    "paths": [
                        paths
                    ],
                    "spatialReference":
                        {
                            "wkid": 32629
                        }
                    };
                totalUpdates += 1;
            }

            setTimeout(function(){ 
                fetch(ID); 
            }, time);
        });
    }

    client.query('SELECT "ID" FROM "Device" WHERE "UserID" = 66', function(err, result) {
        if (err) {
            console.log(err);
            return; 
        }

        result.rows.forEach(function(elem) {
            devices[elem.ID] = {};
            fetch(elem.ID);
        });
    });

    var httpServer = http.createServer(function(request, response) {
        totalRequests += 1;

        response.writeHead(200, {
            "Content-Type": "application/json", 
            "Access-Control-Allow-Origin": "http://tracker.munin.fo",
            "Access-Control-Allow-Headers": "X-Requested-With"
        });

        var urlobj = url.parse(request.url, true).query;

        if (devices[urlobj.deviceid]) {
            response.write(JSON.stringify(devices[urlobj.deviceid]));
        }

        console.log(request.connection.remoteAddress);
        response.end();

    }).listen(8001);

    function status() {
        console.log('\u001B[2J\u001B[0;0f');
        console.log("Runtime: " + Math.round((new Date() - startTime)/1000) + " seconds");
        console.log("Total HTTP requests: " + totalRequests);
        console.log("Total database queries: " + totalUpdates);
        console.log();
        console.log("Memory usage: " + util.inspect(process.memoryUsage()));

        setTimeout(status, 1000);
    }
    status();

