#!/usr/bin/python
# -*- coding: utf-8 -*-

#---------------------------------------------------------------------#
# Laguage: Python 2.7                                                 #
#                                                                     #
# Description:                                                        #
#                Python script updates the attribute of feature class #
# which is published on arcgis server. The attribute is updated via   #
# passing a array of features in JSON format to REST API.             #
#                                                                     #
# This scripts connect to Arcgis service by use of TOKEN              #
#                                                                     #
# Date: 14. march 2014, Martin Sirkovsky, Munin                       #
#---------------------------------------------------------------------#


import httplib, json, urllib2, urllib






#----------------#
# USER INPUT     
#                

# CREDINTIALS used to generate token
username = "Torshavnar"
password = "snarskivan150"

username = "tkr"
password = "tkai"

# URL to feature service endpoint which will be updated.
serviceEndpoint = "http://srv8.kort.fo/arcgis/rest/services/torshavnar/torshavnar_aarinntok/FeatureServer/0/"

# Name of the attribute which will be used for the feature selection
queryAttribute = "upprid"

# Name of the attribute (column name) which is going to be updated
changeAttribute = "vidmerking_felt"

#                
# end USER INPUT 
#----------------#







#----------------#
# DEBUGING
#                

# Value for the attribute selection query 
selectValue = 3

# Value to update
updateValue = "Ok, ok, ok"

# Where clause used for selection
whereClause = queryAttribute + "=" + str(selectValue)


#                
# end DEBUGING
#----------------#



def getToken(username, password, serverName):
    # Token URL is typically http://server[:port]/arcgis/admin/generateToken
    tokenURL = "/arcgis/tokens/generateToken"
    
    # URL-encode the token parameters:-
    params = urllib.urlencode({'username': username, 'password': password, 'client': 'requestip', 'f': 'json', 'expiration': 1,})
    
    headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
    
    # Connect to URL and post parameters
    httpConn = httplib.HTTPSConnection(serverName)
    httpConn.request("POST", tokenURL, params, headers)
    
    # Read response
    response = httpConn.getresponse()

    if (response.status != 200):
        httpConn.close()
        print "Error while fetch tokens from admin URL. Please check the URL and try again."
        return
    else:
        data = response.read()
        httpConn.close()
           
        # Extract the token from it
        token = json.loads(data)        
        return token['token']

token = getToken(username, password, "srv8.kort.fo")
print token





# Get the feature based on the attribute selection. The feature attribute(s) will be modified and updated


urlString = serviceEndpoint + "query?token=" + token + "&where=" + whereClause + "&f=pjson&outFields=*"
response = urllib2.urlopen(urlString).read()


# Convert the returned response to python dictionary format
selectedFeature = json.loads(response)

# Extract just the information about the feature from the response
selectedFeature = selectedFeature['features'][0]

# Delete the geometry definition (only attributes are going to be updated.)
selectedFeature.pop("geometry")

print "Current feature attributes: "
print selectedFeature

# Change the defined attribute to defined value
print "\nModified feature attributes: "
selectedFeature['attributes'][changeAttribute] = updateValue
print selectedFeature


# Modify the changed feature attributes to format specified by REST API
selectedFeature = {"features":[selectedFeature]}

# Convert it to JSON object
packet = json.dumps(selectedFeature["features"])

# Preapare the data to be sent to rest api
data = urllib.urlencode({'token':token, 'features': packet, 'f': 'json', 'rollbackOnFailure': True})
print data

# Send the data and recieve the response
#urlString = serviceEndpoint + "updateFeatures"
#response = urllib2.urlopen(urlString, data).read()


#print response
