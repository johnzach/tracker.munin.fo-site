-- Function: admin.aarinntok_uppgavu_trigger()

-- DROP FUNCTION admin.aarinntok_uppgavu_trigger();


CREATE OR REPLACE FUNCTION admin.aarinntok_uppgavu_trigger()
  RETURNS trigger AS
$BODY$import httplib

host = "tracker-munin-fo-ltyvlejym1i8.runscope.net"
tablename = "a565"

# Set upprid if it is not set, at terminate.
# When setting the upprid, the trigger will run again (it runs on update)
if TD["new"]["upprid"] is None:
  upprid = TD["new"]["objectid"]
  update_uppr_id_plan = plpy.prepare("UPDATE admin." + tablename + " SET upprid = $1 WHERE objectid = $1", ["integer"])
  plpy.execute(update_uppr_id_plan, [upprid])
  return



# Define feature class name which is being processed
featureClass = "torshavnar_aarinntok_uz"


# Lookup x,y values for location
plan = plpy.prepare("SELECT st_x(shape) x, st_y(shape) y FROM admin." + tablename + "  WHERE objectid = $1", [ "integer" ])
row = plpy.execute(plan, [TD["new"]["objectid"]], 1)


# Init dictionary to hold the value key pairs
valueKeyPairs = {}

# Add key value pairs which will be constat for current record
valueKeyPairs["featureclass"] = featureClass
valueKeyPairs["orig_id"] = str(TD["new"]["upprid"])
valueKeyPairs["task_user"] = str(TD["new"]["syningarfolk"])
valueKeyPairs["task_user_comment"] = str(TD["new"]["vidmerking_felt"])

# Location data
valueKeyPairs["locations[0][geometry][x]"] = str(row[0]["x"])
valueKeyPairs["locations[0][geometry][y]"] = str(row[0]["y"])
valueKeyPairs["locations[0][displayname]"] = "Áarinntak"


# If requied fields have null values, it indicates that a feature is not fully created.
if TD["new"]["navn"] is None:
  return


# Iterate over the statuses for grabbast, reinsasts og umvælast
columnsToProcess = ["grabbast","reinsast","umvaelast"]

for column in columnsToProcess:

    # Add value key pairs specific for each colum
    valueKeyPairs["description"] = column + " - " + str(TD["new"]["navn"])
    valueKeyPairs["task_type"] = column
    valueKeyPairs["task_value"] = str(TD["new"][column])

    # Build the string to pass in URL
    taskObjectString = ""

    # Iterate the items in dictionary and create a string with object parameters
    for key, value in valueKeyPairs.iteritems():
        taskObjectString += "&" + key + "=" +str(value)

    # Send a custom string - DEBUGGING
    # taskObjectString = "&description="+"Test Martin: " + column


    # SEND THE DATA
    try:
        
        sendData = "token=46Xkhujpe6LikU6axtqlB1UUV/1mdmoqOJZRl2pIXjST8yGtYWTJfrTMjgnHsteF" + taskObjectString
        # Connect to customer config page to determine the customers config
        conn = httplib.HTTPConnection(host)
        conn.set_debuglevel(1)
        conn.connect()
        print "Connected"
        conn.putrequest("POST", "/api/tasks")

        conn.putheader('Content-Type',  'application/x-www-form-urlencoded')
        conn.putheader('Content-Length', "%d" % len(sendData))
        conn.endheaders()

        conn.send(sendData)
        conn.close()

        

    except Exception, e:
        print "Feilur: " + str(e)
        raise e$BODY$
  LANGUAGE plpythonu VOLATILE
  COST 100;
ALTER FUNCTION admin.aarinntok_uppgavu_trigger()
  OWNER TO admin;

